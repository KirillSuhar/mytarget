<?php


namespace App\Services;


use App\Exceptions\WebMasterServiceException;
use App\Services\Interfaces\WebMasterServiceInterface;
use Mockery\CountValidator\Exception;
use Illuminate\Http\Request;
use App\Repositories\Interfaces\SiteRepositoryInterface;
use App\Repositories\Interfaces\SitePageRepositoryInterface;
use App\Repositories\Interfaces\BannerPlaceRepositoryInterface;
use App\Repositories\Interfaces\UserRepositoryInterface;
use App\Repositories\Interfaces\BannerShowRequestRepositoryInterface;
use App\Repositories\Interfaces\BannerRepositoryInterface;
use App\Repositories\Interfaces\CategoryRepositoryInterface;
use App\Common\Enums\SiteStatus;
use App\Common\Enums\BannerShowRequestStatus;

class WebMasterService implements WebMasterServiceInterface
{
    private $user_repo;
    private $site_repo;
    private $page_repo;
    private $banner_place_repo;
    private $banner_show_request_repo;
    private $banner_repo;
    private $category_repo;

    public function  __construct(UserRepositoryInterface $user_repo,
                                 SiteRepositoryInterface $site_repo,
                                 SitePageRepositoryInterface $page_repo,
                                 BannerPlaceRepositoryInterface $banner_place_repo,
                                 BannerShowRequestRepositoryInterface $banner_show_request_repo,
                                 BannerRepositoryInterface $banner_repo,
                                 CategoryRepositoryInterface $category_repo){
        $this->user_repo = $user_repo;
        $this->site_repo = $site_repo;
        $this->page_repo = $page_repo;
        $this->banner_place_repo = $banner_place_repo;
        $this->banner_show_request_repo = $banner_show_request_repo;
        $this->banner_repo = $banner_repo;
        $this->category_repo = $category_repo;
    }
    private function arrayToJson($data)
    {
        for($i = 0; $i<count($data);$i++)
        {
            $data[$i] =  $data[$i]->toJson();
        }
        return $data;
    }
    
    public function getAllBannerRequests($user_id,$page_size){
        try {
            $data =  $this->banner_show_request_repo->getRequestsFullInfo($user_id,$page_size);

            return $this->arrayToJson($data);
        }
        catch(DALException $e){
            $message = 'Error while creating withdraw money request(DAL Error)';
            throw new WebMasterServiceException($message,0,$e);
        }
        catch(Exception $e){
            $message = 'Error while creating withdraw money request(UnknownError)';
            throw new WebMasterServiceException($message,0,$e);
        }
    }

    private  function validateNewSiteInput(Request $request)
    {
        $messages=array(
            'name.required'=>'Поле name должно быть заполнено',
            'domain.required'=>'Поле domain должно быть заполнено',
            'name.max:100'=>'name должно содержать до 100 символов',
            'name.min:2'=>'name должно содержать от 2 символов',
            'doman.max:255'=>'domain должен содержать до 255 символов',
            'domain.min:2'=>'domain должен содержать от 2 символов'
        );
        try {

            $this->validator = Validator::make($request->all(), ['name' => 'required|max:100|min:2',
                                                                 'domain' => 'required|max:255|min:2'
                                                                ], $messages);
            if($this->validator->fails())
            {
                return $this->validator->messages();
            }
        }
        catch(Exception $e){
            $message = 'Validation error';
            throw new WebMasterServiceException($message,0,$e);
        }
    }

    public function addNewSite(Request $request){
        try {
            $messages = $this->validateNewSiteInput($request);
            if(!empty($messages)){
                return $messages;
            }
            $request['status'] = 0;
            $this->site_repo->create($request);
        }
        catch(DALException $e){
            $message = 'Error while creating withdraw money request(DAL Error)';
            throw new WebMasterServiceException($message,0,$e);
        }
        catch(Exception $e){
            $message = 'Error while creating withdraw money request(UnknownError)';
            throw new WebMasterServiceException($message,0,$e);
        }
    }

    public function getSitesFullInfo($webmaster_id,$page_size){
        try {
           $data = $this->site_repo->getSiteFullInfo($webmaster_id,$page_size);
          
            return $data;
        }
        catch(DALException $e){
            $message = 'Error while getting sites(DAL Error)';
            throw new WebMasterServiceException($message,0,$e);
        }
        catch(Exception $e){
            $message = 'Error while getting sites(UnknownError)';
            throw new WebMasterServiceException($message,0,$e);
        }
    }
   


    private function filterForbiddenData($data){
        unset($data['id_webmaster']);
        unset($data['id_category']);
        unset($data['name']);
        unset($data['domain']);
        unset($data['description']);
        unset($data['status']);
        return $data;
    }

    public function editSite($data,$site_id){
        try {
            $data['status']=SiteStatus::ACTIVE;
            $this->filterForbiddenData($data);
            $this->site_repo->update($data, $site_id);
        }
        catch(DALException $e){
            $message = 'Error while creating withdraw money request(DAL Error)';
            throw new WebMasterServiceException($message,0,$e);
        }
        catch(Exception $e){
            $message = 'Error while creating withdraw money request(UnknownError)';
            throw new WebMasterServiceException($message,0,$e);
        }
    }

    public function acceptRequest($show_request_id){
        try {
             $this->banner_show_request_repo->update(['status' => BannerShowRequestStatus::ACCEPTED],$show_request_id);
        }
        catch(DALException $e){
            $message = 'Error while creating withdraw money request(DAL Error)';
            throw new WebMasterServiceException($message,0,$e);
        }
        catch(Exception $e){
            $message = 'Error while creating withdraw money request(UnknownError)';
            throw new WebMasterServiceException($message,0,$e);
        }
    }

    public function rejectRequest($show_request_id){
        try {
            $this->banner_show_request_repo->update(['status' => BannerShowRequestStatus::DECLINED],$show_request_id);
        }
        catch(DALException $e){
            $message = 'Error while creating withdraw money request(DAL Error)';
            throw new WebMasterServiceException($message,0,$e);
        }
        catch(Exception $e){
            $message = 'Error while creating withdraw money request(UnknownError)';
            throw new WebMasterServiceException($message,0,$e);
        }
    }

    public function addSitePages($site_id){ throw new Exception();}





 

}