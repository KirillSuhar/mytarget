<?php


namespace App\Services\Interfaces;


interface AdminServiceInterface
{
    public function getWithdrawMoneyRequest($id);
}