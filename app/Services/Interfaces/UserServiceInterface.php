<?php


namespace App\Services\Interfaces;


interface UserServiceInterface
{
    public function getProfileData($id);

    public function setProfileData($id,$data);

    public function getMoneyOperationsHistory($id);

    public function getCurrentBalance($id);

    public function createMoneyWithdrawRequest($id,$money,$qiwi);
    
    //заглушка
    public function fillBalance($id);

}