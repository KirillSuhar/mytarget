<?php


namespace App\Services\Interfaces;


interface SiteStatisticsServiceInterface
{
    public static function getLikes($url);
    
    public static function getYandexIndexedCount($url);
    
    public static function getTIC($url);
    
    public static function getAGS($url);
    
    public static function getGooglePageRank($url);
    
    public static function getDmoz($url);
    
    public static function getDomainCreationDate($url);
    
    public static function getAll($url);
    
}