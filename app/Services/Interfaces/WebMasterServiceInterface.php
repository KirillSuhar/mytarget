<?php


namespace App\Services\Interfaces;



use Illuminate\Http\Request;



interface WebMasterServiceInterface
{

    public function getAllBannerRequests($user_id,$page_size);

    public function addNewSite(Request $request);

    public function addSitePages($site_id);

    public function getSitesFullInfo($webmaster_id,$page_size);

    public function editSite($data,$id);

    public function acceptRequest($show_request_id);
    
    public function rejectRequest($show_request_id);




}