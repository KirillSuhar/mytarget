<?php


namespace App\Services;
use App\Services\Interfaces\SiteStatisticsServiceInterface;
use App\Common\JsonRpcClient;
use App\Common\ApiToken;

class SiteStatisticsService implements SiteStatisticsServiceInterface
{

   public static function getLikes($url){
       $api = new JsonRpcClient('http://seozoo.ru/api');
       $response = $api->getLikes(array(
           'host' => $url,
           'token' => ApiToken::getToken()
       ));
       return $response;
   }

    public static function getYandexIndexedCount($url){
        $api = new JsonRpcClient('http://seozoo.ru/api');
        $response = $api->getYandexPagesInIndex(array(
            'data' => array(
                'url' => $url
            ),
            'token' => ApiToken::getToken()
        ));
        return $response;
    }

    public static function getTIC($url){
        $api = new JsonRpcClient('http://seozoo.ru/api');
        $response = $api->getCy(array(
            'data' => array(
                'url' => $url
            ),
            'token' => ApiToken::getToken()
        ));
        return $response;
    }

    public static function getAGS($url){
        $api = new JsonRpcClient('http://seozoo.ru/api');
        $response = $api->getYandexAgs(array(
            'data' => array(
                'url' => $url
            ),
            'token' => ApiToken::getToken()
        ));
        return $response;
    }

    public static function getGooglePageRank($url){
        $api = new JsonRpcClient('http://seozoo.ru/api');
        $response = $api->getPagerank(array(
            'data' => array(
                'url' => $url
            ),
            'token' => ApiToken::getToken()
        ));
        return $response;

    }
    public static function getDmoz($url){
        $api = new JsonRpcClient('http://seozoo.ru/api');
        $response = $api->getDmoz(array(
            'data' => array(
                'url' =>  $url
            ),
            'token' => ApiToken::getToken()
        ));
        return $response;

    }

    public static function getAll($url)
    {
        $result = ['likes' => SiteStatisticsService::getLikes($url),
                   'yandex_pages_count' => SiteStatisticsService::getYandexIndexedCount($url),
                   'tic' => SiteStatisticsService::getTIC($url),
                   'ags' => SiteStatisticsService::getAGS($url),
                   'google_page_rank' => SiteStatisticsService::getGooglePageRank($url),
                   'dmoz' => SiteStatisticsService::getDmoz($url),
                   'domen_age' => SiteStatisticsService::getDomainCreationDate($url)];
        return $result;
    }

    public static function getDomainCreationDate($url){
        $request = 'http://htmlweb.ru/analiz/api.php?whois&url='.$url.'&json';
        $ch = curl_init($request);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); // Возвращаем данные, а не выводим на экран.
        $result = curl_exec($ch);
        $decoded = json_decode($result);
        $domain_creation_date = $decoded->creation;
        curl_close($ch);
        return $domain_creation_date;
    }

}