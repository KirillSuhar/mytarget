<?php


namespace App\Services;
use App\Exceptions\DALException;
use App\Exceptions\RegistrationServiceException;
use App\Services\Interfaces\RegistrationServiceInterface;
use App\Repositories\Interfaces\UserRepositoryInterface;
use App\Repositories\Interfaces\ConfirmUserRepositoryInterface;
use Auth;
use App\Common\Enums\UserRole;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Validator;
use Mail;
use Hash;


class RegistrationService implements RegistrationServiceInterface
{
    private $user_repo;
    private $confirm_user_repo;

    private $validator;

    public function __construct(UserRepositoryInterface $user_repo,
                                ConfirmUserRepositoryInterface $confirm_user_repo)

    {
        $this->user_repo = $user_repo;
        $this->confirm_user_repo = $confirm_user_repo;

    }

    public function registerConfirmUser(Request $request)
    {
        try {
            $messages = $this->validateRegisterConfirmUserInput($request);
            if(!empty($messages)) {
                return $messages;
            }
            $email_exists = $this->checkIfUserEmailExists($request->email);

            if ($email_exists) {
                return ['other' => 'Пользователь с данным email уже существует'];
            }
            $token = $this->generateToken();
            $this->createConfirmUser($request, $token);
            $this->sendEmail($token);

            return ['other' => 'token: '.$token];//здесь должно быть сообщение об успешной отправке мыла, но пусть пока токен будет
        }
        catch(Exception $e) {
            $message = 'Unique link sending failed';
            throw new RegistrationServiceException($message,0,$e);
        }


    }
    public function checkIfConfirmUserExist($token)
    {
        try {
            if ($this->confirm_user_repo->findBy('token', $token) != null) {
                return true;
            }
            return false;
        }
        catch(DALException $e){
            $message = 'Error while checking ConfirmUser(DAL Error)';
            throw new RegistrationServiceException($message,0,$e);
        }
        catch(Exception $e){
            $message = 'Error while checking ConfirmUser(Unknown Error)';
            throw new RegistrationServiceException($message,0,$e);
        }
    }

    public function register(Request $request){
        try {

            if(!$this->checkIfConfirmUserExist($request->token)) return ['other' => 'Неверный код верификации.'];

            $messages = $this->validateRegisterInput($request);

            if(!empty($messages)) {
                return $messages;
            }
            $login_exists = $this->checkIfLoginExists($request->login);

            if ($login_exists) {
                return ['other' => 'Пользователь с данным логином уже существует'];
            }
            $data = $this->getConvertedUserData($request);

            $this->user_repo->createUserAndDeleteConfirmUserTransaction($data,$request->token);
            Auth::attempt(['email' => $data['email'], 'password' => $request->password], true);
            $this->setRole();

            return;
        }
        catch(Exception $e) {
            $message = 'Registration failed';
            throw new RegistrationServiceException($message,0,$e);
        }
    }

    private function setRole(){
        switch (Auth::user()->role){
            case 0: Auth::guard('webmaster')->login(Auth::user()); break;
            case 1: Auth::guard('advertiser')->login(Auth::user()); break;
            case 2: Auth::guard('moderator')->login(Auth::user()); break;
            case 3: Auth::guard('admin')->login(Auth::user()); break;
        }
    }

    private  function validateRegisterConfirmUserInput(Request $request)
    {
        $messages=array(
            'email.required'=>'Поле E-mail должно быть заполнено',
            'email.email'=>'E-mail должен быть настоящим адресом',
            'email.max:255'=>'E-mail должен содержать до 255 символов'
        );
        try {
            $this->validator = Validator::make($request->all(), ['email' => 'required|email|max:255'], $messages);
            if($this->validator->fails())
            {
                return $this->validator->messages();
            }
        }
        catch(Exception $e){
            $message = 'Validation error';
            throw new RegistrationServiceException($message,0,$e);
        }
    }
    private  function generateToken()
    {
        $token = str_random(32);
        return $token;
    }
    private  function checkIfUserEmailExists($email)
    {
        try {
            if ($this->user_repo->findBy('email', $email) != null) {
                return true;
            }
            return false;
        }
        catch(DALException $e){
            $message = 'Error while Email Checking (DAL Error)';
            throw new RegistrationServiceException($message,0,$e);
        }
        catch(Exception $e){
            $message = 'Error while Email Checking (Unknown Error)';
            throw new RegistrationServiceException($message,0,$e);
        }
    }
    private  function sendEmail($token)
    {
        try {
            Mail::raw('mytarget.ru/confirm/' . $token, function ($message) {
                $message->from("mytargetmy@gmail.com", "MyTarget");
                $message->subject("Подтверждение адреса");
                $message->to(Input::get('email'));
            });
        }
        catch(Exception $e){
            $message =  'Error mail sending in RegistrationService';
            throw new RegistrationServiceException($message,0,$e);
        }
    }
    private  function createConfirmUser(Request $request,$token)
    {
        try {
            $email = $request->email;
            $confirm_user = $this->confirm_user_repo->findBy('email', $email);
            if ($confirm_user == null) {
                $this->confirm_user_repo->create([
                    'email' => $email,
                    'token' => $token
                ]);
            } else {
                $this->confirm_user_repo->update(['token' => $token], $confirm_user['id']);
            }
        }
        catch(DALException $e){
            $message = 'Error while creating ConfirmUser(DAL Error)';
            throw new RegistrationServiceException($message,0,$e);
        }
        catch(Exception $e) {
            $message = 'Error while creating ConfirmUser(Unknown Error)';
            throw new RegistrationServiceException($message,0,$e);
        }

    }

    private function getConvertedUserData(Request $request)
    {

        $role = UserRole::ADVERTISER;

        if($request->optionsRadios == "webmaster"){
            $role = UserRole::WEBMASTER;
        }
        $result =  $this->confirm_user_repo->findBy('token',$request->token,'=','email');

        $full_name = $request->surname.' '.$request->name.' '.$request->patronymic;
        $data = [ 'full_name' => $full_name,
                  'name'  => $request->login,
                  'email' => $result['email'],
                  'password' => bcrypt($request->password),
                  'user_qiwi' => $request->qiwi,
                  'role' => strval($role),
                  'balance' => '0',
                  'frozen_balance' => '0',
        ];

        return $data;
    }



    private function getFormErrorMessages()
    {
        $messages=array(
            'surname.required'=>'Поле "Фамилия" должно быть заполнено',
            'surname.max:50'=>'Фамилия должна быть не больше 50 символов',
            'surname.alpha'=>'Фамилия должна содержать только буквы',
            'name.required'=>'Поле "Имя" должно быть заполнено',
            'name.max:50'=>'Имя должно быть не больше 50 символов',
            'name.alpha'=>'Имя должно содержать только буквы',
            'patronymic.required'=>'Поле "Отчество" должно быть заполнено',
            'patronymic.max:50'=>'Отчество должно быть не больше 50 символов',
            'patronymic.alpha'=>'Отчество должна содержать только буквы',
            'login.required'=>'Поле "Логин" должно быть заполнено',
            'login.max:16'=>'Логин должен быть не больше 16 символов',
            'password.required'=>'Поле "Пароль" должно быть заполнено',
            'password.between'=>'Пароль должен содержать от 4 до 16 символов',
            'passwordAgain.required'=>'Поле "Повторите пароль" должно быть заполнено',
            'passwordAgain.same:password'=>'Пароль должен совпадать',


        );
        return $messages;
    }
    private function validateRegisterInput(Request $request)
    {
        $messages = $this->getFormErrorMessages();
        try {
            $this->validator = Validator::make($request->all(), [
                'surname' => 'required|max:50|alpha',
                'name' => 'required|max:50|alpha',
                'patronymic' => 'required|max:50|alpha',
                'login' => 'required|max:16',
                'password' => 'required|between:4,16',
                'passwordAgain' => 'required|same:password',


            ], $messages);
            if($this->validator->fails())
            {
                return $this->validator->messages();
            }
        }
        catch(Exception $e){
            $message = 'Validation error';
            throw new RegistrationServiceException($message,0,$e);
        }
    }
    private  function checkIfLoginExists($login)
    {
        try {
            if ($this->user_repo->findBy('name', $login) != null) {
                return true;
            }
            return false;
        }
        catch(DALException $e){
            $message = 'Error while Login Checking (DAL Error)';
            throw new RegistrationServiceException($message,0,$e);
        }
        catch(Exception $e){
            $message = 'Error while Login Checking (Unknown Error)';
            throw new RegistrationServiceException($message,0,$e);
        }
    }

}