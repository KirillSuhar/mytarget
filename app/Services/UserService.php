<?php


namespace App\Services;
use App\Exceptions\DALException;
use App\Exceptions\UserServiceException;
use App\Repositories\Interfaces\WithdrawMoneyRequestRepositoryInterface;
use App\Services\Interfaces\UserServiceInterface;
use App\Repositories\Interfaces\UserRepositoryInterface;
use App\Repositories\Interfaces\MoneyOperationRepositoryInterface;
use App\Common\Enums\WithdrawMoneyRequestStatus;

class UserService implements UserServiceInterface
{
    private $user_repo;
    private $money_operations_repo;
    private $withdraw_request_repo;

    public function __construct(UserRepositoryInterface $user_repo,
                                MoneyOperationRepositoryInterface $money_operations_repo,
                                WithdrawMoneyRequestRepositoryInterface $withdraw_request_repo){
        $this->user_repo = $user_repo;
        $this->money_operations_repo = $money_operations_repo;
        $this->withdraw_request_repo = $withdraw_request_repo;
    }



    public function getProfileData($id){
        try {
            $user_data = $this->user_repo->find($id);
            $splitted_full_name = explode(' ',$user_data['full_name']);

            $user_data['surname'] = $splitted_full_name[0];
            $user_data['real_name'] = $splitted_full_name[1];
            $user_data['patronymic'] = $splitted_full_name[2];
            
            return $user_data;
        }
        catch(DALException $e){
            $message = 'Error while getting user data(DAL Error)';
            throw new UserServiceException($message,0,$e);
        }
        catch(Exception $e){
            $message = 'Error while getting user data(UnknownError)';
            throw new UserServiceException($message,0,$e);
        }
    }
    

    private function filterForbiddenData($data){
        unset($data['remember_token']);
        unset($data['email']);
        unset($data['password']);
        unset($data['user_qiwi']);
        unset($data['role']);
        unset($data['balance']);
        unset($data['frozen_balance']);
        return $data;
    }
    public function setProfileData($id,$data){
        try{
            $data = $this->filterForbiddenData($data);
            $this->user_repo->update($data, $id);
        }
        catch(DALException $e){
            $message = 'Error while setting profile data(DAL Error)';
            throw new UserServiceException($message,0,$e);
        }
        catch(Exception $e){
            $message = 'Error while setting user data(UnknownError)';
            throw new UserServiceException($message,0,$e);
        }
    }

    public function getMoneyOperationsHistory($id){
        try{
            $money_operations_data = $this->money_operations_repo->getMoneyOperations($id,10);
            return $money_operations_data;
        }
        catch(DALException $e){
            $message = 'Error while getting money operation history(DAL Error)';
            throw new UserServiceException($message,0,$e);
        }
        catch(Exception $e){
            $message = 'Error while getting money operation history(UnknownError)';
            throw new UserServiceException($message,0,$e);
        }
    }

    public function getCurrentBalance($id){
        try{
            return $this->user_repo->find($id,['balance']);
        }
        catch(DALException $e){
            $message = 'Error while getting user balance(DAL Error)';
            throw new UserServiceException($message,0,$e);
        }
        catch(Exception $e){
            $message = 'Error while getting user balance(UnknownError)';
            throw new UserServiceException($message,0,$e);
        }
    }

    private function checkIfUserHaveEnoughMoney($id,$money)
    {
        $user_data = $this->user_repo->find($id);
        if($user_data == null) {
            throw new UserServiceException('User undefined');
        }
        if($user_data['balance']<$money){
            return false;
        }
        return true;

    }

    // TO DO: реализовать метод проверки существования счета киви
    private function checkIfQiwiExist($qiwi)
    {
        return true;
    }
    public function createMoneyWithdrawRequest($id,$money,$qiwi)
    {
        try{
            if(!$this->checkIfUserHaveEnoughMoney($id, $money)){
                throw new UserServiceException('User has`t got enough money');
            }
            if(!$this->checkIfQiwiExist($qiwi)){
                throw new UserServiceException('Qiwi number not exist');
            }
            $this->withdraw_request_repo->create([
            'user_id'=>$id,
            'output_money'=>$money,
            'status' => WithdrawMoneyRequestStatus::CONSIDER,
            'user_qiwi' => $qiwi
            ]);
        }
        catch(DALException $e){
            $message = 'Error while creating withdraw money request(DAL Error)';
            throw new UserServiceException($message,0,$e);
        }
        catch(Exception $e){
            $message = 'Error while creating withdraw money request(UnknownError)';
            throw new UserServiceException($message,0,$e);
        }
    }
    public function fillBalance($id){
        //TO DO: реализовать пополнение баланса через киви
        //сюда по идее должен приходить id транзакции, и по проверке успешной транзакции qiwi
        //нужно будет добавить деньги во внутренний баланс пользователя
    }
}