<?php


namespace App\Repositories;
use App\Repositories\Core\Repository;
use App\Repositories\Interfaces\SiteBannerSubjectRepositoryInterface;

class SiteBannerSubjectRepository extends Repository implements SiteBannerSubjectRepositoryInterface
{
    function model()
    {
        return 'App\Models\SiteBannerSubject';
    }
}