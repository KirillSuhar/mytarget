<?php


namespace App\Repositories;
use App\Repositories\Core\Repository;
use App\Repositories\Interfaces\SiteStatisticsRepositoryInterface;

class SiteStatisticsRepository extends Repository implements SiteStatisticsRepositoryInterface
{
    function model()
    {
        return 'App\Models\SiteStatistics';
    }
}