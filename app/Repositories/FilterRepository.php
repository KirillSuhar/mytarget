<?php


namespace App\Repositories;
use App\Repositories\Core\Repository;
use App\Repositories\Interfaces\FilterRepositoryInterface;

class FilterRepository extends Repository implements FilterRepositoryInterface
{
    function model()
    {
        return 'App\Models\Filter';
    }
}