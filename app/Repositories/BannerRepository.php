<?php


namespace App\Repositories;
use App\Repositories\Core\Repository;
use App\Repositories\Interfaces\BannerRepositoryInterface;
use App\Common\Enums\BannerStatus;
use App\Models\Banner;
use Exception;
use App\Exceptions\DALException;

class BannerRepository extends Repository implements BannerRepositoryInterface
{

    function model()
    {
        return 'App\Models\Banner';
    }

    public function getActiveBannersByPage($id_page){
        try {
            $banners = Banner::
            join('banner_show_requests', 'banners.id', '=', 'banner_show_requests.id_banner')
                ->join('banner_places', 'banner_show_requests.id_place', '=', 'banner_places.id')
                ->join('site_pages', 'banner_places.id_page', '=', 'site_pages.id')
                ->where('site_pages.id', $id_page)
                ->whereOr('banner_show_requests.status', BannerStatus::ACTIVE)
                ->select('banners.name as banner_name',
                    'banners.domain as banner_domain',
                    'banners.size as banner_size',
                    'banners.click_price',
                    'banners.show_price',
                    'banners.max_clicks',
                    'banners.max_shows',
                    'banners.link')
                ->get();
        }
        catch(Exception $e) {
            $message = 'Error while finding Banner using '.$this->model();
            throw new DALException($message,0,$e);
        }

        if($banners!=null){
            return $banners->toArray();
        }
        return array();
     
    }
}