<?php


namespace App\Repositories;
use App\Repositories\Core\Repository;
use App\Repositories\Interfaces\MoneyOperationRepositoryInterface;

class MoneyOperationRepository extends Repository implements MoneyOperationRepositoryInterface
{
    function model()
    {
        return 'App\Models\MoneyOperation';
    }
    
    public function getMoneyOperations($user_id,$perPage){
        try {
            $data = $this->model->where('id_sender',$user_id)->paginate($perPage);
        }
        catch(Exception $e) {
            $message = 'Error in paginate method in '.$this->model();
            throw new DALException($message,0,$e);
        }
        if($data!=null)
        {
            for($i = 0; $i < count($data);$i++)
                $data[$i]=$data[$i]->toArray();
            
            return $data;
        }
        return array();
    }
    
}