<?php


namespace App\Repositories;
use App\Repositories\Core\Repository;
use App\Repositories\Interfaces\BannerSubjectRepositoryInterface;

class BannerSubjectRepository extends Repository implements BannerSubjectRepositoryInterface
{
    function model()
    {
        return 'App\Models\BannerSubject';
    }
}