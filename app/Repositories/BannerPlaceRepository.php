<?php


namespace App\Repositories;
use App\Repositories\Core\Repository;
use App\Repositories\Interfaces\BannerPlaceRepositoryInterface;

class BannerPlaceRepository extends Repository implements BannerPlaceRepositoryInterface
{
    function model()
    {
        return 'App\Models\BannerPlace';
    }
}