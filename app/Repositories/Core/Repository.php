<?php

namespace App\Repositories\Core;


use Exception;
use App\Exceptions\DALException;
use App;
use Illuminate\Database\Eloquent\Model;
use App\Repositories\Interfaces\RepositoryInterface;


abstract class Repository implements RepositoryInterface
{

    protected $model;

    public function __construct() {

        $this->makeModel();
    }

    abstract function model();
    
    public function where($attribute, $value, $operator = '=', $columns = array('*')) {
        try {
            $data = $this->model->where($attribute, $operator, $value)->get();
        }
        catch(Exception $e) {
            $message = 'Error while finding element using '.$this->model();
            throw new DALException($message,0,$e);
        }
        if($data!=null) {
            return $data->toArray();
        }
        return array();

    }
    
    public function all($columns = array('*')) {
        try{
        $data =  $this->model->get($columns);
        }
        catch(Exception $e){
            $message = 'Error in all() method in '.$this->model();
            throw new DALException($message,0,$e);
        }
        if($data!=null)
        {
            return $data->toArray();
        }
        return array();
    }

    public function paginate($perPage = 15, $columns = array('*')) {
        try {
            $data = $this->model->paginate($perPage, $columns);
        }
        catch(Exception $e) {
            $message = 'Error in paginate method in '.$this->model();
            throw new DALException($message,0,$e);
        }
        if($data!=null)
        {
            return $data->toArray();
        }
        return array();
    }

    public function create(array $data) {
        return $this->model->insert($data);
    }

    public function update(array $data, $id, $attribute="id") {
        try {
            return $this->model->where($attribute, '=', $id)->update($data);
        }
        catch(Exception $e){
            $message = 'Error while updating element using '.$this->model();
            throw new DALException($message,0,$e);
        }
    }

    public function delete($id) {
        try {
            return $this->model->delete($id);
        }
        catch(Exception $e) {
            $message = 'Error while deleting element using '.$this->model();
            throw new DALException($message,0,$e);
        }
    }

    public function find($id, $columns = array('*')) {
        try{
            $data = $this->model->find($id, $columns);
        }
        catch(Exception $e) {
            $message = 'Error while finding element using '.$this->model();
            throw new DALException($message,0,$e);
        }
        if($data!=null)
        {
            return $data->toArray();
        }
        return array();


    }

    public function findBy($attribute, $value, $operator = '=', $columns = array('*')) {
        try {
            $data = $this->model->where($attribute, $operator, $value)->select($columns)->first();
        }
        catch(Exception $e) {
            $message = 'Error while finding element using '.$this->model();
            throw new DALException($message,0,$e);
        }
        if($data!=null) {
            return $data->toArray();
        }
        return array();
    }

    public function makeModel() {
        $model = App::make($this->model());

        if (!$model instanceof Model)
            throw new Exception("Class {$this->model()} must be an instance of Illuminate\\Database\\Eloquent\\Model");

        return $this->model = $model->newQuery();
    }
}