<?php


namespace App\Repositories\Interfaces;


interface BannerRepositoryInterface extends RepositoryInterface
{
    public function getActiveBannersByPage($id_page);
}