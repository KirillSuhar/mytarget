<?php


namespace App\Repositories\Interfaces;


interface SitePageRepositoryInterface extends RepositoryInterface
{
    public function getBannerRequestData($page_id);
}