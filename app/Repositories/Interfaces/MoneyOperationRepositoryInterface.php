<?php


namespace App\Repositories\Interfaces;


interface MoneyOperationRepositoryInterface extends RepositoryInterface
{
    public function getMoneyOperations($user_id,$perPage);
}