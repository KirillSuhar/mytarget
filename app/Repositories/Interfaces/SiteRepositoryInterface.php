<?php


namespace App\Repositories\Interfaces;


interface SiteRepositoryInterface extends RepositoryInterface
{
    public function getSiteFullInfo($user_id,$page_size);
}