<?php


namespace App\Repositories\Interfaces;


interface BannerShowRequestRepositoryInterface extends RepositoryInterface
{
    public function getRequestsFullInfo($id_user,$page_size);
}