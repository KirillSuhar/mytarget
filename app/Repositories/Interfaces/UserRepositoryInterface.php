<?php
namespace App\Repositories\Interfaces;


interface UserRepositoryInterface extends RepositoryInterface
{

    public function createUserAndDeleteConfirmUserTransaction($user_data, $confirm_user_token);

}