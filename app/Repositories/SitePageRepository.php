<?php


namespace App\Repositories;
use App\Repositories\Core\Repository;
use App\Repositories\Interfaces\SitePageRepositoryInterface;
use App\Common\Enums\BannerShowRequestStatus;

class SitePageRepository extends Repository implements SitePageRepositoryInterface
{
    function model()
    {
        return 'App\Models\SitePage';
    }

    public function getBannerRequestData($page_id){
        try {
            $banner_requests = \DB::table('banners')
                ->join('banner_show_requests', 'banners.id', '=', 'banner_show_requests.id_banner')
                ->join('banner_places', 'banner_show_requests.id_place', '=', 'banner_places.id')
                ->join('site_pages', 'banner_places.id_page', '=', 'site_pages.id')
                ->where('site_pages.id', $page_id)
                ->whereOr('banner_show_requests.status', BannerShowRequestStatus::ACCEPTED)
                ->select('banners.name as banner_name',
                    'banners.domain as banner_domain',
                    'banners.size as banner_size',
                    'banners.click_price',
                    'banners.show_price',
                    'banners.max_clicks',
                    'banners.max_shows',
                    'banners.link',
                    'banner_show_requests.clicks_left',
                    'banner_show_requests.shows_left',
                    'banner_show_requests.deadline',
                    'banner_show_requests.status',
                    'banner_show_requests.id as request_id')
                ->get();
        }

        catch(Exception $e) {
            $message = 'Error while finding element using '.$this->model();
            throw new DALException($message,0,$e);
        }
        if($banner_requests !=null) {
            for ($i = 0; $i < count($banner_requests); $i++) {
                $banner_requests[$i] = (array)$banner_requests[$i];
            }
            return $banner_requests;
        }
        return array();

    }
}