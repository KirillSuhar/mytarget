<?php


namespace App\Repositories;
use App\Repositories\Core\Repository;
use App\Repositories\Interfaces\UserRepositoryInterface;
use DB;
use App\Models\ConfirmUser;
use App\Common\Enums\BannerShowRequestStatus;

class UserRepository extends Repository implements UserRepositoryInterface
{

    function model()
    {
        return 'App\Models\User';
    }


    public function createUserAndDeleteConfirmUserTransaction($user_data,$confirm_user_token)
    {
        DB::transaction(function () use ($user_data, $confirm_user_token) {
            $data = ConfirmUser::where('token', '=', $confirm_user_token)->select('id')->first();
            ConfirmUser::destroy($data['id']);

            $this->create($user_data);
        });
    }
  
}