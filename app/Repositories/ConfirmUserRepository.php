<?php


namespace App\Repositories;
use App\Repositories\Core\Repository;
use App\Repositories\Interfaces\ConfirmUserRepositoryInterface;

class ConfirmUserRepository extends Repository implements ConfirmUserRepositoryInterface
{
    function model()
    {
        return 'App\Models\ConfirmUser';
    }
}