<?php


namespace App\Repositories;
use App\Repositories\Core\Repository;
use App\Repositories\Interfaces\SiteRepositoryInterface;
use App\Models\Site;
use App\Models\Banner;
use App\Common\Enums\BannerStatus;

class SiteRepository extends Repository implements SiteRepositoryInterface
{
    function model()
    {
        return 'App\Models\Site';
    }
    public function getSiteFullInfo($user_id,$page_size){
      //  \DB::connection()->enableQueryLog();
        $data = Site::where('id_webmaster','=',$user_id)->with(['sitePages.bannersInfo' => function ($query) {
            $query->join('banners','banner_show_requests.id_banner','=','banners.id')
                ->where('banner_show_requests.status','=',BannerStatus::ACTIVE)
                ->select('banners.name as banner_name',
                    'banners.domain as banner_domain',
                    'banners.size as banner_size',
                    'banners.click_price',
                    'banners.show_price',
                    'banners.max_clicks',
                    'banners.max_shows',
                    'banners.link',
                    'banner_show_requests.clicks_left',
                    'banner_show_requests.shows_left',
                    'banner_show_requests.deadline',
                    'banner_show_requests.status as request_status',
                    'banner_show_requests.id as request_id');
        }])->paginate($page_size);
        if($data == null){
            return array();
        }

        for($i=0;$i<count($data);$i++){
            $data[$i] = $data[$i]->toArray();
        };
        return $data;
       // dd(\DB::connection()->getQueryLog());
    }
}