<?php


namespace App\Repositories;
use App\Repositories\Core\Repository;
use App\Repositories\Interfaces\SystemMoneyOperationRepositoryInterface;

class SystemMoneyOperationRepository extends Repository implements SystemMoneyOperationRepositoryInterface
{
    function model()
    {
        return 'App\Models\SystemMoneyOperation';
    }
}