<?php


namespace App\Repositories;
use App\Repositories\Core\Repository;
use App\Repositories\Interfaces\CategoryRepositoryInterface;
use App\Models\Category;

class CategoryRepository extends Repository implements CategoryRepositoryInterface
{
    function model()
    {
        return 'App\Models\Category';
    }
    public function getAllCategories(){
        try{
        $all_categories_tree = Category::getTree();
        }

        catch(Exception $e) {
            $message = 'Error while finding element using '.$this->model();
            throw new DALException($message,0,$e);
        }
        if($all_categories_tree !=null) {

            return $all_categories_tree->toArray();
        }
        return array();
    }

}