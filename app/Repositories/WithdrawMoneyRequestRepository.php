<?php


namespace App\Repositories;
use App\Repositories\Core\Repository;
use App\Repositories\Interfaces\WithdrawMoneyRequestRepositoryInterface;

class WithdrawMoneyRequestRepository extends Repository implements WithdrawMoneyRequestRepositoryInterface
{
    function model()
    {
        return 'App\Models\WithdrawMoneyRequest';
    }
}