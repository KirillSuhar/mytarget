<?php


namespace App\Repositories;
use App\Repositories\Core\Repository;
use App\Repositories\Interfaces\ProjectRepositoryInterface;

class ProjectRepository extends Repository implements ProjectRepositoryInterface
{
    function model()
    {
        return 'App\Models\Project';
    }
}