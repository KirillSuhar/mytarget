<?php


namespace App\Repositories;
use App\Repositories\Core\Repository;
use App\Repositories\Interfaces\MessageRepositoryInterface;

class MessageRepository extends Repository implements MessageRepositoryInterface
{
    function model()
    {
        return 'App\Models\Message';
    }
}