<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Auth;

class EntryController extends Controller
{
    public function index()
    {
        if(Auth::check()){
            return redirect('/webmaster');
        }
        return view('entry');
    }
}
