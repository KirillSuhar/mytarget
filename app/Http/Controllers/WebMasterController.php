<?php


namespace App\Http\Controllers;

use App\Http\Requests\Request;
use App\Services\Interfaces\WebMasterServiceInterface;
use App\Services\Interfaces\UserServiceInterface;
use App\Exceptions\WebMasterServiceException;
use Auth;
use Exception;

class WebMasterController extends Controller
{
    private $webmaster_service;

    public function __construct(WebMasterServiceInterface $webmaster_service)
    {
        $this->webmaster_service = $webmaster_service;

    }

    public function getAllBannerRequests()
    {

        $page_size = 3;

        $user_id = Auth::user()->id;
        $fake = 5;
        $response = $this->webmaster_service->getAllBannerRequests($user_id, $page_size);
        return view('webmaster\bannerTable', ['response' => $response]);

    }

    public function addNewSite(Request $request)
    {
        try {
            $errors = $this->webmaster_service->addNewSite($request);
            if (!empty($errors)) {
                return redirect('/webmaster_add_site')->withErrors($errors)->withInput();
            }
            return $errors;
        } catch (WebMasterServiceException $e) {
            return 'Ошибка при добавлении сайта. Временная заглушка.';
        } catch (Exception $e) {
            return 'Неизвестная ошибка добавлении сайта. Временная заглушка.';
        }


    }

    public function getSites()
    {

        $fake = 5;
        $user_id = Auth::user()->id;

        $page_size = 3;
        $response = $this->webmaster_service->getSitesFullInfo($user_id,$page_size);
        return view('webmaster/mySites', ['data' => $response]);
    }

    public function editSite(Request $request)
    {
        $responce = $this->webmaster_service->editSite($request);
        return redirect('/webmaster_site_edit')->withInput();
    }

    public function acceptRequest($request_id)
    {
        $this->webmaster_service->acceptRequest($request_id);
    }

    public function rejectRequest($request_id)
    {
        $this->webmaster_service->rejectRequest($request_id);
    }
}