<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\Interfaces\RegistrationServiceInterface;
use App\Exceptions\RegistrationServiceException;
use Exception;
use App\Http\Requests;
use Auth;

class RegistrationController extends Controller
{
    private $reg_service;


    public function __construct(RegistrationServiceInterface $register_service)
    {
        $this->reg_service = $register_service;
    }

    public function registerConfirmUser(Request $request)
    {
        try {
            $errors = $this->reg_service->registerConfirmUser($request);
            if(!empty($errors)) {
                return redirect('/entry')->withErrors($errors)->withInput();
            }
            //здесь должен быть редирект на страницу,где написано, что письмо успешно отправлено
            //и типа там будет кнопка "На главную"
            return $errors;
        }
        catch(RegistrationServiceException $e) {
            return 'Ошибка при регистрации. Временная заглушка.';
        }
        catch(Exception $e) {
            return 'Неизвестная ошибка регистрации. Временная заглушка.';
        }
    }

    public function confirm($token)
    {
        try {
            $token_exist = $this->reg_service->checkIfConfirmUserExist($token);
            if(!$token_exist){ return 'токена нет';}
            return view('registration',['token' => $token]);
        }
        catch(RegistrationServiceException $e) {
            return 'Ошибка при подтверждении мыла. Временная заглушка.';
        }
        catch(Exception $e) {
            return 'Неизвестная ошибка при подтверждении мыла. Временная заглушка.';
        }
    }

    public function register(Request $request)
    {
        $messages = $this->reg_service->register($request);

        if(!empty($messages)) {
            
            return redirect('/confirm/'.$request->token)->withErrors($messages)->withInput($request->toArray());
        }
        return  redirect ('/webmaster');


    }
    
    
}
