<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Services\Interfaces\UserServiceInterface;
use Exception;
use App\Exceptions\AuthServiceException;
use App\Http\Requests;
use App\Models\User;
use Auth;

class UserController extends Controller
{
    private $user_service;

    public function __construct(UserServiceInterface $user_service) {

        $this->user_service = $user_service;
    }

    public function index()
    {
        $user = Auth::user();
        $data = $this->user_service->getProfileData($user->id);
        return view('webmaster\profile',$data);
    }

    public function getMoneyOperations(Request $request){
        $user = Auth::user();
        $data = $this->user_service->getMoneyOperationsHistory($user->id);
        return view('webmaster\moneyHistory',['data' => $data]);
    }

    public function cashOuts(){
        $user = Auth::user();
        $data = $this->user_service->getProfileData($user->id);
        return view('webmaster\cashouts',['data' => $data]);
    }

    public function createMoneyWithdrawRequest(Request $request){
        $user = Auth::user();
            $this->user_service->createMoneyWithdrawRequest($user->id, $request->money, $user->user_qiwi);
            return redirect('webmaster\cashouts');

    }
    public function addSite()
    {
        return view('webmaster\addSite');
    }
}
