<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');

});
Route::get('/forgetpassword',function(){
    return view('forgetPassword');
});
Route::get('/instruction', function () {
    return view('instruction');
});

Route::get('/newPassword',function(){
    return view('newPassword');
});

Route::get('/rules', function () {
    return view('rules');
});

Route::get('/webmaster',  ['middleware' => ['auth','checkRole:webmaster'], function () {
    return view('webmaster/main');
}]);

Route::get('/webmaster_bannerTable', 'WebMasterController@getAllBannerRequests');

Route::get('/webmaster_mysites', 'WebMasterController@getSites');

Route::get('/webmaster_test', function () {
    return view('webmaster/test');
});
Route::get('/webmaster_addSite', 'UserController@addSite');

Route::get('/webmaster_moneyHistory','UserController@getMoneyOperations');

Route::get('/webmaster_cashouts','UserController@cashOuts');

Route::get('/withdrawRequest','UserController@createMoneyWithdrawRequest');


Route::get('/webmaster_addSite', 'UserController@addSite');

Route::get('/webmaster_profile', 'UserController@index');

Route::get('/home', 'HomeController@index');

Route::get('/test','TestController@index');

Route::get('/usertest','UserController@index');

Route::get('/entry','EntryController@index');

Route::post('/login','Auth\AuthController@login');

Route::get('/logout','Auth\AuthController@logout');

Route::get('/confirm/{token}','RegistrationController@confirm');

Route::post('/registerConfirmUser','RegistrationController@registerConfirmUser');

Route::post('/register','RegistrationController@register');

Route::any('/webmaster/acceptRequest/{id}','WebMasterController@acceptRequest');

Route::get('/webmaster/rejectRequest/{id}','WebMasterController@rejectRequest');



Route::post('/reset','Auth\PasswordController@forgetAccept@reset');

Route::post('/newPassword','Auth\PasswordController@forget');

Route::get('/reset/{token}','Auth\PasswordController@forgetAccept');

Route::post('/forget','Auth\PasswordController@resetForget');





//Route::get('/', ['uses' => 'ModalEntryRegistrWindowController@index', 'as' => 'modal']);