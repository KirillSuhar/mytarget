<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class BackendServiceProvider extends ServiceProvider
{
    public function boot(){
        
    }
    public function bindRepositories()
    {
        $this->app->bind( 'App\Repositories\Interfaces\UserRepositoryInterface',
                          'App\Repositories\UserRepository' );

        $this->app->bind( 'App\Repositories\Interfaces\BannerPlaceRepositoryInterface',
                          'App\Repositories\BannerPlaceRepository' );

        $this->app->bind( 'App\Repositories\Interfaces\BannerRepositoryInterface',
                          'App\Repositories\BannerRepository' );

        $this->app->bind( 'App\Repositories\Interfaces\BannerShowRequestRepositoryInterface',
                          'App\Repositories\BannerShowRequestRepository');

        $this->app->bind( 'App\Repositories\Interfaces\BannerSubjectRepositoryInterface',
                          'App\Repositories\BannerSubjectRepository' );

        $this->app->bind( 'App\Repositories\Interfaces\CategoryRepositoryInterface',
                          'App\Repositories\CategoryRepository' );

        $this->app->bind( 'App\Repositories\Interfaces\FilterRepositoryInterface',
                          'App\Repositories\FilterRepository' );

        $this->app->bind( 'App\Repositories\Interfaces\MessageRepositoryInterface',
                          'App\Repositories\MessageRepository' );

        $this->app->bind( 'App\Repositories\Interfaces\MoneyOperationRepositoryInterface',
                          'App\Repositories\MoneyOperationRepository' );

        $this->app->bind( 'App\Repositories\Interfaces\ProjectRepositoryInterface',
                          'App\Repositories\ProjectRepository' );

        $this->app->bind( 'App\Repositories\Interfaces\SiteBannerSubjectRepositoryInterface',
                          'App\Repositories\SiteBannerSubjectRepository' );

        $this->app->bind( 'App\Repositories\Interfaces\SitePageRepositoryInterface',
                          'App\Repositories\SitePageRepository' );

        $this->app->bind( 'App\Repositories\Interfaces\SiteRepositoryInterface',
                          'App\Repositories\SiteRepository' );

        $this->app->bind( 'App\Repositories\Interfaces\SiteStatisticsRepositoryInterface',
                          'App\Repositories\SiteStatisticsRepository' );
        
        $this->app->bind( 'App\Repositories\Interfaces\SystemMoneyOperationRepositoryInterface',
                          'App\Repositories\SystemMoneyOperationRepository' );

        $this->app->bind( 'App\Repositories\Interfaces\WithdrawMoneyRequestRepositoryInterface',
                          'App\Repositories\WithdrawMoneyRequestRepository' );

        $this->app->bind( 'App\Repositories\Interfaces\ConfirmUserRepositoryInterface',
                          'App\Repositories\ConfirmUserRepository' );

        $this->app->bind( 'App\Repositories\Interfaces\PasswordResetRepositoryInterface',
                          'App\Repositories\PasswordResetRepository' );

        $this->app->bind( 'App\Services\Interfaces\AuthorizationServiceInterface',
                          'App\Services\AuthorizationService' );
        
        $this->app->bind( 'App\Services\Interfaces\UserServiceInterface',
                          'App\Services\UserService' );

        $this->app->bind( 'App\Services\Interfaces\AdminServiceInterface',
                          'App\Services\AdminService' );

        $this->app->bind( 'App\Services\Interfaces\SiteStatisticsServiceInterface',
                          'App\Services\SiteStatisticsService' );

        $this->app->bind( 'App\Services\Interfaces\RegistrationServiceInterface',
                          'App\Services\RegistrationService' );
        

        $this->app->bind( 'App\Services\Interfaces\WebMasterServiceInterface',
                          'App\Services\WebMasterService' );

        $this->app->bind( 'App\Services\Interfaces\PasswordServiceInterface',
                          'App\Services\PasswordService' );
    }
    public function register()
    {
        $this->bindRepositories();
    }
}