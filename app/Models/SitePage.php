<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SitePage extends Model
{
    public function bannersInfo(){
        return $this->hasManyThrough('App\Models\BannerShowRequest','App\Models\BannerPlace','id_page','id_place');
    }
    public function bannerPlaces(){
        return $this->hasMany('App\Models\BannerPlace','id_page');
    }
}
