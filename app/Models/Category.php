<?php
namespace App\models;

use Franzose\ClosureTable\Models\Entity;

class category extends Entity implements categoryInterface
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'categories';

    /**
     * ClosureTable model instance.
     *
     * @var categoryClosure
     */
    protected $closure = 'App\models\categoryClosure';
}
