<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BannerPlace extends Model
{
    public function bannerShowRequests(){
        return $this->hasMany('App\Models\BannerShowRequest','id_place');
    }
    public function banners(){
        return $this->belongsToMany('App\Models\Banner','banner_show_requests','id_place','id_banner');
    }
}
