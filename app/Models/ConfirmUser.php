<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ConfirmUser extends Model
{
    protected $fillable =  ['email', 'token'];
    protected $hidden = ['token'];
    protected $table = 'confirm_users';
    //
}
