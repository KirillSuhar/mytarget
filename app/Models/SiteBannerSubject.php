<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SiteBannerSubject extends Model
{
    public  $timestamps = false;
}
