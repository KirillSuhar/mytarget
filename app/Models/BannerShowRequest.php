<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BannerShowRequest extends Model
{
    public function banner(){
        return $this->belongsTo('App\Models\Banner','id_banner');
    }
}
