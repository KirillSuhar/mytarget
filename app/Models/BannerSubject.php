<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BannerSubject extends Model
{
    public  $timestamps = false;
}
