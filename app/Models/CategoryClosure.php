<?php
namespace App\models;

use Franzose\ClosureTable\Models\ClosureTable;

class categoryClosure extends ClosureTable implements categoryClosureInterface
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'category_closure';
}
