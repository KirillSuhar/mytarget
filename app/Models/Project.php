<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    public function user(){
        $this->belongsTo('App\Models\User','id_advertiser');
    }
}
