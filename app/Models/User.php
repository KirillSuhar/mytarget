<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public function sites(){
        return $this->hasMany('App\Models\Site','id_webmaster');
    }
    public function projects()
    {
        return $this->hasMany('App\Models\Projects','id_advertiser');
    }
    protected $fillable = [
        'full_name','name', 'email', 'password','user_qiwi'
    ];
    protected $guarded = ['id'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token'
    ];
}
