<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{
    public function project()
    {
        return $this->belongsTo('App\Models\Project','id_banner');
    }
}
