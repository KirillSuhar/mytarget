<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PasswordReset extends Model
{
    protected $fillable =  ['email', 'token'];
    protected $hidden = ['token'];
    protected $table = 'password_resets';
    //
}
