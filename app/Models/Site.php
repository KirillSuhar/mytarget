<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Site extends Model
{
    public function sitePages(){
        return $this->hasMany('App\Models\SitePage','id_site');
    }
}
