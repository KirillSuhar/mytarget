<?php


namespace App\Common\Enums;
use App\Common\Enum;

class MessageStatus extends Enum
{
    const READ = 1;
    const UNREAD = 0;
}