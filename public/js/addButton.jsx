/**
 * Created by Карина on 10.07.2016.
 */

var CollapsedMenu = React.createClass({
    getInitialState: function () {
        return {
            isClicked: false,
            isBodyClicked: false,
            isOpened: false
        }
    },

    componentDidMount: function () {
        document.body.addEventListener('click', this.handleBodyClicked);
    },
    componentWillUnmount: function () {
        document.body.removeEventListener('click', this.handleBodyClicked);
    },

    handleBodyClicked: function () {
        if(this.state.isClicked && !this.state.isBodyClicked) {
            this.setState({
                isBodyClicked: true,
                isOpened: false
            })
        }
        else if(!this.state.isClicked && !this.state.isBodyClicked) {
            this.setState({
                isBodyClicked: false,
                isOpened: false,
                isClicked: false
            })
        }
        else if(this.state.isClicked && this.state.isBodyClicked) {
            this.setState({
                isBodyClicked: false,
                isOpened: false,
                isClicked: false
            })
        }
    },

    handleClick: function () {
        if (!this.state.isClicked) {
            this.setState({
                isClicked: true,
                isOpened: true
            })
        }
    },

    menuOpened: function () {
        return  <div>
            <button type="button" className="navbar-toggle collapsed" id="btn-toggle" onClick={this.handleClick}>
                <span className="glyphicon glyphicon-menu-hamburger"></span>
            </button>
            <div className="div-collapsed-menu">
                <div className="div-collapsed-menu-titles">
                    <a href="instruction">Инструкция</a> <br/>
                    <a href="rules">Правила</a><br/>
                    <a href="login">Вход</a><br/>
                </div>
            </div>
        </div>

    },

    menuClosed: function () {
        return <div>
            <button type="button" className="navbar-toggle collapsed" id="btn-toggle" onClick={this.handleClick}>
                <span className="glyphicon glyphicon-menu-hamburger"></span>
            </button>
        </div>
    },

    render: function () {
        if (this.state.isOpened) return this.menuOpened();
        else return this.menuClosed();
    }
});

ReactDOM.render(
    <h1>dddddddd</h1>,
    document.getElementById("ex")
);

