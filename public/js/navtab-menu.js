/**
 * Created by Карина on 13.07.2016.
 */

$('#myTabs a').click(function (e) {
    e.preventDefault();

    var url = $(this).attr("data-url");
    var href = this.hash;
    var pane = $(this);

    $(href).load(url,function(result){
        pane.tab('show');
    });
});


$('#profile').load($('.active a').attr("data-url"),function(result){
    $('.active a').tab('show');
});

