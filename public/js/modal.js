$(document) .on('show.bs.modal', function (e)
{
    var thisDialog = $(e.target).find('.modal-dialog');
    $(e.target).css('display','block');

    $(window).bind("resize.modalAlign", function ()
    {
        thisDialog.css('margin-top', (thisDialog .outerHeight() < $(window).height()) ? (($(window).height() - thisDialog.outerHeight()) / 2 + 'px') : '')
    })

        .resize();
})
