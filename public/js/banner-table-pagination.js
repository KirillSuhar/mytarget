/**
 * Created by Карина on 16.07.2016.
 */


$('#pagination a').on('click', function(e){
    e.preventDefault();

    var hrefSplitted = $(this).attr('href').split("=");

    $.ajax({
        url: ($('.banner-applications-content').attr('data-url')+"?page="+hrefSplitted[1]).replace(/\s+/g, ''),
        success: function(data) {
            $('.banner-applications-content').html(data);
            $.getScript("js/profile-modal-windows.js");
            $.getScript("js/banner-table-pagination.js");
        }
    });
});