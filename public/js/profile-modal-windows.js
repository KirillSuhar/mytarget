/**
 * Created by Карина on 14.07.2016.
 */

$(document).ready(function(){

    $(".banner").click(function(){
        var modal = $("#infoModal");
        modal.modal();
        modal.find("#infoModal-name").html(
        '<span class="glyphicon glyphicon-picture"></span> Баннер'
        );
        modal.find("#infoModal-content").html(
            '<dl class="dl-horizontal dl-banner" >' +
            '        <dt>Название: </dt> ' +
            '        <dd id="name"></dd> ' +
            '            <dt>Домен: </dt> ' +
            '        <dd id="domen"></dd> ' +
            '            <dt>Размер: </dt> ' +
            '        <dd id="size"></dd> ' +
            '            <dt>Цена за клик: </dt> ' +
            '        <dd id="click-price"></dd> ' +
            '            <dt>Цена за показ: </dt> ' +
            '        <dd id="show-price"></dd> ' +
            '            <dt>Максимальное количесто кликов: </dt> ' +
            '        <dd id="max-click"></dd> ' +
            '            <dt>Максимальное количество показов: </dt> ' +
            '        <dd id="max-show"></dd> ' +
            '            <dt>Название страницы: </dt> ' +
            '        <dd id="page-name"></dd> ' +
            '            </dl>'
        )
        
        var values = $(this).attr("content");

        data = JSON.parse(values);
        document.getElementById("name").innerText = data.banner_name;
        document.getElementById("domen").innerText = data.banner_domain;
        document.getElementById("size").innerText = data.banner_size.replace(/,/g, "");
        document.getElementById("click-price").innerText =data.click_price;
        document.getElementById("show-price").innerText = data.show_price;
        document.getElementById("max-click").innerText = data.max_clicks;
        document.getElementById("max-show").innerText = data.max_shows;
        document.getElementById("page-name").innerText = data.page_name;
    });



    $(".btn-accept").click(function(){
        var modal = $("#yesNoModal");
        modal.modal();
        var values = $(this).attr("content");
        var data = JSON.parse(values);

        modal.find("#yesNoModal-content").html("Вы точно хотите это сделать?");
        modal.find("#yesNoModal-btn-ok").click(function () {
            $.ajax({
                type: 'get',
                async: false,
                url: ('webmaster/acceptRequest/' + data.request_id),
                success: function (data) {
                    modal.hide();
                    var modalSuccess = $("#yesModal");
                    modalSuccess.modal();
                    document.getElementById("yesModal-content").innerHTML = "Заявка успешно принята!";
                    $.ajax({
                        url: ('/webmaster_bannerTable'),
                        success: function(data) {
                            $('.banner-applications-content').html(data);
                            $.getScript("js/profile-modal-windows.js");
                            $.getScript("js/banner-table-pagination.js");
                        }
                    });
                }
            })
        })
    });



    $(".btn-reject").click(function(){
        var modal = $("#yesNoModal");
        modal.modal();
        var values = $(this).attr("content");
        var data = JSON.parse(values);

        modal.find("#yesNoModal-content").html("Вы точно хотите это сделать?");
        modal.find("#yesNoModal-btn-ok").click(function () {
            $.ajax({
                type: 'get',
                async: false,
                url: ('webmaster/rejectRequest/' + data.request_id),
                success: function (data) {
                    modal.hide();
                    var modalSuccess = $("#yesModal");
                    modalSuccess.modal();
                    document.getElementById("yesModal-content").innerHTML = "Заявка успешно отклонена!";
                    $.ajax({
                        url: ('/webmaster_bannerTable'),
                        success: function(data) {
                            $('.banner-applications-content').html(data);
                            $.getScript("js/profile-modal-windows.js");
                            $.getScript("js/banner-table-pagination.js");
                        }
                    });
                }
            })
        })
    });







    
    
    
    




});