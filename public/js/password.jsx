var React = require('react');
var Layout = require('ui-components/layouts');
var Form = require('ui-components/form');
var TextInput = require('ui-components/text-input');
var validator = require('validator');
var dispatcher = require('validator');

var FormScreen = React.createClass({
    
    formns: "newPas",
    componentWillMount: function () {
        dispatcher.on(this.formns, this.listener);
    },
    componentWillUnmount: function () {
        dispatcher.off(this.formns, this.listener);
    },

    getValidationSchema: function () {
        return {
            pas1: [
                {
                    validate: function (value) {
                        return validator.isLength(value, 6, 10);
                    },
                    message: "Не менее 6 символов"
                }
            ],
            pas2: [
                {
                    validate: function (value) {
                        return validator.isLength(value, 6, 10);
                    },
                    message: "Не менее 6 символов"
                },
                {
                    validate: validator.isEmail,
                    message: "Пожалуйста, укажите корректный email"
                }
            ]
        }
    },
    submit: function(model) {
    },

    listener: function (event) {
        if (event.command == 'model_update') {
            this.setState({model: event.model});
        }
    },
    render: function() {

        var surname = (this.state.model && this.state.mode.pas1) ? (
            <TextInput formns={this.formns} name="pas2"/>
        ) : null;

        return (
            <Layout>
                <Form formns={this.formns} onSubmit={this.submit} schema={this.getValidationSchema}>
                    <TextInput formns={this.formns} name="name" title="Имя" />
                    {surname}
                    <TextInput formns={this.formns} name="email" title="Email" />
                </Form>
            </Layout>
        );
    }
});

module.exports = FormScreen;