@extends('layouts.app')

@section('menu')

    <div class="container-fluid">
        <ul class="nav nav-tabs" id="myTabs">
            <li class="active"><a href="#home" data-url="{{url('/webmaster_test')}}">Home</a></li>
            <li><a href="#profile" data-url="/embed/62806/view">Profile</a></li>
            <li><a href="#messages" data-url="/embed/62807/view">Messages</a></li>
        </ul>

        <div class="tab-content">
            <div class="tab-pane active" id="home">This is the home pane...</div>
            <div class="tab-pane" id="profile"></div>
            <div class="tab-pane" id="messages"></div>
        </div>
    </div>

    <script type="text/javascript" src={{asset('js/navtab-menu.js')}}></script>

@endsection