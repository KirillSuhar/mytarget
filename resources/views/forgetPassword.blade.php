@extends('layouts.app')

@section('title', 'Забыли пароль')

@section('css')
    <link rel="stylesheet" href={{asset('css/forgetPassword.css')}}>
@endsection

@section('content')
        <div class="container ">
            <div class="row"> </div>
                 <div class="col-md-8 col-md-offset-3">
                     <div class="panel panel-default">
                         <div class="panel-body">
                             <h2 class="text-center"> Восстановление доступа к странице </h2>
                                <form  method="post" action="{{ url('/newPassword') }}">
                                    <div class="col-md-10 col-md-offset-1 ">
                                        <div class="form-group form-inline">
                                            <label for="themeTextField" style="margin-right: 15px">Ваш e-mail: </label>
                                            <div class="input-group">
                                                <input type="text" class="form-control hoverHint " name="email" id="themeTextField" >
                                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                 <span class="input-group-btn">
                                                     <button class="btn btn-default" type="submit"> ОК</button>
                                                 </span>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                         </div>
                     </form>
                  </div>
             </div>
        </div>

@endsection
