@extends('layouts.app')

@section('title', 'Новый пароль')

@section('css')
    <link rel="stylesheet" href={{asset('css/moneyHistory.css')}}>
    <link rel="stylesheet" href={{asset('css/forgetPassword.css')}}>
@endsection

@section('content')
    <div class="container ">
        <div class="row"></div>
        <div class="col-sm-8 col-sm-offset-3">
            <div class="panel panel-default">
                <div class="panel-body">
                    <h2 class="text-center"> Восстановление пароля </h2>
                    <div class="col-sm-8 col-sm-offset-2">
                        <form role="form" name="newPas" class="newpas" method="post" action="{{ url('/forget') }}">
                            <div class="form-group ">
                                <label for="password1"> Введите новый пароль: </label>
                                <input class="form-control" name="password" type="password"
                                       placeholder="не менее 6 символов" id="password1">
                            </div>
                            <div class="form-group ">
                                <label for="password2"> Повторите новый пароль: </label>
                                <input class="form-control" name="passwordAgain" type="password" id="password2">
                            </div>
                            <input type="hidden" name="token" value="{{ $token }}">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <button class="btn btn-default bttn" type="submit"> Ок</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/babel" src={{asset('js/password.jsx')}}></script>
    <script src="http://fb.me/react-0.10.0.min.js"></script>
@endsection