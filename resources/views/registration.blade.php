@extends('layouts.app')

@section('title', 'Регистрация')
@section('css')
    <link rel="stylesheet" href={{asset('css/registration.css')}}>
@endsection

@section('content')

<div class="container ">
    <h2 class="text-center">Регистрация</h2>
    <form class="form-horizontal" role="form" method="POST" action="{{ url('/register') }}">
        {{ csrf_field() }}
        <div class="form-group{{ $errors->has('surname') ? ' has-error' : '' }}">
            <label for="inputSurname" class="col-sm-2 control-label">Фамилия: *</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="inputSurname" name="surname" placeholder="Фамилия" value="{{ old('surname') }}">
                @if ($errors->has('surname'))
                    <span class="help-block">
                                            <strong>{{ $errors->first('surname') }}</strong>

                    </span>
                @endif
            </div>
        </div>
        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
            <label for="inputName" class="col-sm-2 control-label">Имя: *</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="inputName" name="name" placeholder="Имя" value="{{ old('name') }}">
                @if ($errors->has('name'))
                    <span class="help-block">
                                            <strong>{{ $errors->first('name') }}</strong>

                    </span>
                @endif
            </div>
        </div>
        <div class="form-group{{ $errors->has('patronymic') ? ' has-error' : '' }}">

            <label for="inputPatronymic" class="col-sm-2 control-label">Отчество: *</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="inputPatronymic" name="patronymic" placeholder="Отчество" value="{{ old('patronymic') }}">
                @if ($errors->has('patronymic'))
                    <span class="help-block">
                                            <strong>{{ $errors->first('patronymic') }}</strong>
                                        </span>
                @endif
            </div>
        </div>
        <div class="form-group{{ $errors->has('login') ? ' has-error' : '' }}">
            <label for="inputLogin" class="col-sm-2 control-label">Логин: *</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="inputLogin" name="login" placeholder="Логин" value="{{ old('login') }}">
                @if ($errors->has('login'))
                    <span class="help-block">
                                            <strong>{{ $errors->first('login') }}</strong>
                                        </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">

            <label for="inputPassword" class="col-sm-2 control-label">Пароль: *</label>
            <div class="col-sm-10">
                <input type="password" class="form-control" id="inputPassword" name="password" placeholder="Пароль" >
                @if ($errors->has('password'))
                    <span class="help-block">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('passwordAgain') ? ' has-error' : '' }}">

            <label for="inputPasswordAgain" class="col-sm-2 control-label">Повторите пароль: *</label>
            <div class="col-sm-10">
                <input type="password" class="form-control" id="inputPasswordAgain" name="passwordAgain" placeholder="Повторите пароль" >
                @if ($errors->has('password'))
                    <span class="help-block">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('qiwi') ? ' has-error' : '' }}">
            <label for="inputQIWI" class="col-sm-2 control-label">Счет QIWI: </label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="inputQIWI" name="qiwi" placeholder="Счет QIWI" value="{{ old('qiwi') }}">
            </div>
        </div>
        <div class="form-group">
         <label class="col-sm-2 control-label">Роль: *</label>
        <div class="radio">
            <label for="optionWebmaster" >
                <input type="radio" name="optionsRadios" id="optionWebmaster" name="webmaster" value="webmaster" checked>
                Вебмастер
            </label>
            <label for="optionAdvertiser">
                <input type="radio" name="optionsRadios" id="optionAdvertiser" name="advertiser" value="advertiser">
                Рекламодатель
            </label>
        </div>
        </div>

        <span id="mark" class="text-left">* - Обязательные поля для заполнения</span>
        <input type="hidden" name="token" value={{$token}}>
        <div class="form-group text-center">
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-default">Зарегистрироваться</button>
            </div>
        </div>
    </form>
</div>

@endsection