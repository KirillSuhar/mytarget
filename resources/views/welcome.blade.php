@extends('layouts.app')

@section('title', 'MyTarget')
@section('css')
    <link rel="stylesheet" href={{asset('css/welcome_page.css')}}>
@endsection

@section('content')
    <!-- Carousel -->
    <div>
        <div id="myCarousel" class="carousel slide" data-ride="carousel" data-intervak="3000" data-pause="false">
            <div class="carousel-inner" role="listbox">
                <div class="item active one">
                    <div id="first" class="slide" ></div>
                </div>
                <div class="item two">
                    <div id="second" class="slide"></div>
                </div>
                <div class="item three">
                    <div id="third" class="slide"></div>
                </div>
            </div>

            <!-- Headline -->
            <div class="headline">
                <h1>Пример заголовка страницы</h1>
            </div>

            <!-- Button get started -->
            <div class="begin_button">
                @if (Auth::guest())
                    <center><a id="begin_work_button" href="{{ url('/entry') }}">Начать работу</a></center>
                @else
                    <center><a id="begin_work_button" href="{{ url('/') }}">Начать работу</a></center>
                @endif
            </div>
        </div>
    </div>

    <!-- Services -->
    <div class="container service">
        <div class="row service_row">
            <div class="col-md-4">
                <h2>Услуга 1</h2>
                <img src="http://dummyimage.com/250x250/696969/ffffff" alt="Услуга 1" class="img-rounded">
                <p class="">Описание услуги</p>
            </div>
            <div class="col-md-4">
                <h2>Услуга 2</h2>
                <img src="http://dummyimage.com/250x250/696969/ffffff" alt="Услуга 2" class="img-rounded">
                <p>Описание услуги</p>
            </div>
            <div class="col-md-4">
                <h2>Услуга 3</h2>
                <img src="http://dummyimage.com/250x250/696969/ffffff" alt="Услуга 3" class="img-rounded">
                <p>Описание услуги</p>
            </div>
        </div>
    </div>

    <!-- Installation steps -->
    <div class="container installation_steps">
        <div class="row">
            <div class="installation_steps_header">
                <h1 class="text-center">Начать работу за 3 шага</h1>
            </div>
        </div>
        <div class="row installation_steps_row">
            <div class="col-md-4">
                <h2>Шаг 1</h2>
                <img src="http://dummyimage.com/250x250/696969/ffffff" alt="Услуга 1" class="img-circle">
                <p class="">Описание шага</p>
            </div>
            <div class="col-md-4">
                <h2>Шаг 2</h2>
                <img src="http://dummyimage.com/250x250/696969/ffffff" alt="Услуга 2" class="img-circle">
                <p>Описание шага</p>
            </div>
            <div class="col-md-4">
                <h2>Шаг 3</h2>
                <img src="http://dummyimage.com/250x250/696969/ffffff" alt="Услуга 3" class="img-circle">
                <p>Описание шага</p>
            </div>
        </div>
    </div>

    <!-- Contacts -->
    <div class="container map">
        <div class="row">
            {{--<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d1443142.125216778!2d33.80435358034021!3d45.052588423026926!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sru!2sru!4v1468182539026" width="500" height="500" frameborder="0" style="border:0" allowfullscreen></iframe>--}}
            <div class="panel">
                <div class="panel-body">
                    <h1>Контакты</h1>
                    <hr>
                    <h3><span class="glyphicon glyphicon-home"></span> Адрес: <strong id="contacts">ул. Пример д.20</strong></h3>
                    <h3><span class="glyphicon glyphicon-phone-alt"></span> Телефон: <strong id="contacts">+7 (777) 585-58-58</strong></h3>
                </div>
            </div>
        </div>
    </div>

@endsection
