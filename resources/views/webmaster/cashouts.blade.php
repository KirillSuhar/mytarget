
    <link rel="stylesheet" href={{asset('css/moneyHistory.css')}}>

<div class="container webmaster-profile-container">
    <div class="container-fluid profile-head-container">
        <div class="row">
            <h3>ЗАЯВКА НА ВЫВОД СРЕДСТВ</h3>
            <label> Текущий баланс:</label>
              <label> {{ isset($data['balance']) ? $data['balance'] : 'Заполните данные о QIWI кошельке'}} </label>
                  </br>
                  <label> Объем денежных средств для вывода: </label>
                     <form role="form" method="POST" action="{{ url('/recharge') }}">
                         <div class="col-md-2">
                             <span class="input-group">
                                 <input type="text" class="form-control " required pattern="^[ 0-9]+$" placeholder="введите сумму">
                             </span>
                         </div>
                         <button type="submit" class="btn btn-success bttn "> ОК </button>
                     </form>
        </div>
    </div>
    <script type="text/javascript" src={{asset('js/banner-modal-wind.js')}}></script>
</div>


<!-- Modal
<div class="modal fade" id="myModa" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="padding:35px 50px;">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4>Заявка обрабатывает</h4>
                <button type="submit" class="btn-success btn-md "> ОК </button>
            </div>
        </div>

    </div>
</div> -->