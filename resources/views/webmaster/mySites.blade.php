<div class="container webmaster-profile-container">
    <h3>МОИ САЙТЫ</h3>
    <div class="blocks-applications">
        <div class="table-responsive">
            <table class="table table-bordered first-level " id="report">
                <tr>
                    <th>Название</th>
                    <th>Домен</th>
                    <th>Описание</th>
                    <th>Категория</th>
                    <th>Тематика принимаемых баннеров</th>
                    <th></th>
                </tr>
                @for($k=1;$k<4;$k++)
                <tr id="first-level" onclick="click_first_level({{$k}})">
                    <td>Тест</td>
                    <td>Тест</td>
                    <td>Тест</td>
                    <td>Тест</td>
                    <td>Тест</td>
                    <td><div class="arrow" id="arrow-num-{{$k}}" style="background-image: url('{{asset('img/arrows.png')}}');"></div></td>
                </tr>
                <tr class="second-level" id="num-second-{{$k}}">
                    <td colspan="6">
                        <h4 class="text-center">СТРАНИЦЫ</h4>
                    <table class="table table-bordered " id="second-level">
                        <tr >
                            <th>Название</th>
                            <th>Домен</th>
                            <th>Размер принимаемых баннеров</th>
                            <th></th>
                        </tr>
                        @for($i=1;$i<6;$i++)
                        <tr   id="second" onclick="click_second_level({{$i}}{{$k}})">
                            <td>Тест2</td>
                            <td>Тест2</td>
                            <td>Тест2</td>
                            <td><div class="arrow" id="arrow-num-{{$k}}{{$i}}" style="background-image: url('{{asset('img/arrows.png')}}');"></div></td>
                        </tr>
                        <tr class="third-level" id="num-third-{{$i}}{{$k}}">
                            <td colspan="6">
                                <h4  class="text-center">БАННЕРЫ</h4>
                                <table class="table table-bordered" id="third-level">
                                    <tr >
                                        <th>Название</th>
                                        <th>Размер</th>
                                        <th>Цена за клик</th>
                                        <th>Цена за показ</th>
                                        <th>Максимальное количество показов</th>
                                        <th>Максимальное количество кликов</th>
                                    </tr>
                                    <tr id="third">
                                        <td>Тест3</td>
                                        <td>Тест3</td>
                                        <td>Тест3</td>
                                        <td>Тест3</td>
                                        <td>Тест3</td>
                                        <td>Тест3</td>
                                    </tr>
                                </table></td>
                        </tr>@endfor
                    </table>
                        <div class="pull-right">
                            <button type="button" class="btn btn-default"><span class="glyphicon glyphicon-stats"></span> Мониторинг</button>
                            <button type="button" class="btn btn-default"><span class="glyphicon glyphicon-pencil"></span> Редактировать</button>
                            <button type="button" class="btn btn-default btn-danger"><span class="glyphicon glyphicon-trash"></span> Удалить</button>
                        </div>
                    </td>
                </tr>@endfor
            </table>
        </div>
    </div>
</div>
<script type="text/javascript" src={{asset('js/table-folding.js')}}></script>