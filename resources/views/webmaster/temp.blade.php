<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Hello World!</title>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/react/0.14.0/react.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/react/0.14.0/react-dom.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/babel-core/5.6.15/browser.js"></script>
</head>
<body>
<div id='content'></div>

<script type="text/babel">
    var s = React.createClass({
        render: function(){
            return(
                            <div>
                    <button type="button" class="btn btn-sm btn-success" id="addSite">
                    <span class="glyphicon glyphicon-ok"></span> Добавить</button>
                            <br />
                            </div>
            )
        }

    });
    var HelloWorld = React.createClass({
        select: function(event){
         //   return <Button />;
        },
        render: function() {
            return (
                            <div>
                    <select id="thematics" class="form-control" name="thematics" onChange={this.select}>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    </select>


                            </div>
            );
        }
    });
 
    class MyForm extends React.Component {

        constructor(props) {
            super(props);
            //this.handleSubmit = this.handleSubmit.bind(this);
            this.addObj = this.addObj.bind(this);
            this.state = { obj: [] };
        }



        addObj(event) {
            const { obj } = this.state;
            this.setState({ obj: [ ...obj, { name: event.target.value } ] });
        }


        render() {
            const { obj } = this.state;
            const { handleSubmit, addObj } = this;
            return (
                    <form >
                    <select id="thematics" class="form-control" name="thematics" onChange={this.addObj}>
        <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    </select>
                    <div>
                    { obj.map( (obj, i) => <ObjInput key={i} obj={obj} n={i}/> ) }
        </div>
            </form>
        );
        }
    }

    class ObjInput extends React.Component {

        render() {
            return (
                    <div>
                    Количество товара {this.props.obj.name} <input type="text" />
        </div>
        );
        }
    }

    ReactDOM.render(<MyForm /> , document.getElementById('content'));

</script>
</body>
</html>