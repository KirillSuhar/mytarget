<table class="table banner-applications-table  table-bordered">
    <?php
    for($i=0; $i<10; $i++ ) {
        $array[$i] = $i;
    }
    ?>
    @for($i=0; $i < 10; $i++ )
        <tr>
            <td> Рекламодатель </td>
            <td>  <button type="button" class="btn btn-default btn-lg banner" id="{{ $i }}" content="{{ $array[$i] }}" >Баннер</button> </td>
            <td class="table-button"> <button type="button" class="btn btn-sm btn-success btn-ok" id="{{ $i }}">
                    <span class="glyphicon glyphicon-ok"></span> Принять</button> </td>
            <td class="table-button"> <button type="button" class="btn btn-sm btn-danger btn-cancel" id="{{ $i }}">
                    <span class="glyphicon glyphicon-remove"></span> Отклонить</button> </td>
        </tr>
    @endfor

</table>

<script type="text/javascript" src={{asset('js/profile-modal-windows.js')}}></script>