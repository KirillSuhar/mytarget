<div class="container webmaster-profile-container">
    <a>Назад</a>
    <h3>ИЗМЕНЕНИЕ ПАРОЛЯ</h3>
    <form role="form" data-toggle="validator" class="col-md-4">
            <div class="form-group">
                <label for="inputCurrentPassword" class="control-label password">Текущий пароль:</label>
                <input type="password"  class="form-control password" minlength="6" maxlength="15" pattern="^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\s).*$" id="inputCurrentPassword" placeholder="Введите текущий пароль"  required>
                <div class="help-block">Минимум-6 символов, максимум-15</div>
            </div>
            <div class="form-group">
                <label for="inputNewPassword">Новый пароль:</label>
                <input type="password"  minlength="6" maxlength="15" pattern="^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\s).*$" class="form-control password" id="inputNewPassword" placeholder="Введите новый пароль" required>
                <div class="help-block with-errors"></div>
            </div>
            <div class="form-group">
                <label for="inputRepeatNewPassword">Повторите новый пароль:</label>
                <input type="password" data-match="#inputNewPassword" class="form-control" id="inputRepeatNewPassword" placeholder="Введите повторно новый пароль" data-match-error="Пароли не совпадают" required>
                <div class="help-block with-errors"></div>
            </div>
        <button type="submit" class="btn btn-default">ОК</button>
        <button type="reset" class="btn btn-default">ОТМЕНА</button>
    </form>
</div>
<script type="text/javascript" src={{asset('js/validation-tooltip.js')}}></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/1000hz-bootstrap-validator/0.11.5/validator.js"></script>