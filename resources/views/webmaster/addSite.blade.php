

<div class="container">
    <div class="row">
        <div class="col-sm-6">
            <div style="border-width: 0 1px 0 0; border-style: solid;">
                <form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
                    {{ csrf_field() }}

                    <div class="form-group">
                            <h1>Сайт</h1><br>
                    </div>

                    <div class="form-group">
                        <label for="name" class="col-sm-4">Название:</label>

                        <div class="col-sm-6">
                            <input id="name" type="text" class="form-control" name="name">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="domain" class="col-sm-4 ">Домен:</label>

                        <div class="col-sm-6">
                            <input id="domain" type="text" class="form-control" name="domain">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="description" class="col-sm-4 ">Описание:</label>

                        <div class="col-sm-6">
                            <textarea id="description" type="text" class="form-control" name="description" rows="3" maxlength="5">text</textarea>
                        </div>
                    </div>

                    <div class="firstCategories">
                        <div class="form-group">
                            <label for="fcategories" class="col-sm-4">Категория:</label>

                            <div class="col-sm-6">
                                <select id="fcategories" class="form-control" name="fcategories">
                                    <option>1</option>
                                    <option>2</option>
                                    <option>3</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group categoriesContainer">
                            <label for="addfCategories" class="col-sm-4">Выбранные категории:</label>
                            <!-- Реализовать на ReactJS добавление в виде кнопок -->
                        </div>
                    </div>
                    <div class="secondCategories">
                        <div class="form-group">
                            <label for="scategories" class="col-sm-4">Подкатегория:</label>

                            <div class="col-sm-6">
                                <select id="scategories" class="form-control" name="scategories">
                                    <option>1</option>
                                    <option>2</option>
                                    <option>3</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group categoriesContainer">
                            <label for="addsCategories" class="col-sm-4 ">Выбранные подкатегории:</label>
                            <!-- Реализовать на ReactJS добавление в виде кнопок -->
                        </div>
                    </div>
                    <div class="thirdCategories">
                        <div class="form-group">
                            <label for="tcategories" class="col-sm-4">Подподкатегория:</label>

                            <div class="col-sm-6">
                                <select id="tcategories" class="form-control" name="tcategories">
                                    <option>1</option>
                                    <option>2</option>
                                    <option>3</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group categoriesContainer">
                            <label for="addtCategories" class="col-sm-4">Выбранные подподкатегории:</label>
                            <!-- Реализовать на ReactJS добавление в виде кнопок -->
                        </div>
                    </div>

                    <div class="thematics">
                        <div class="form-group">
                            <label for="thematics" class="col-sm-4 ">Тематика добавляемых баннеров:</label>

                            <div class="col-sm-6">
                                <select id="thematics" class="form-control" name="thematics">
                                    <option>1</option>
                                    <option>2</option>
                                    <option>3</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group categoriesContainer">
                            <label for="addthematics" class="col-sm-4">Выбранные тематики:</label>
                            <!-- Реализовать на ReactJS добавление в виде кнопок -->
                        </div>
                    </div>

                        <div class="form-group sitePagesContainer">
                            <label for="sitePages" class="col-sm-4">Добавленные страницы:</label>
                            <!-- Реализовать на ReactJS добавление в виде кнопок -->
                        </div>


                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            <button type="button" class="btn btn-sm btn-success" id="addSite">
                                <span class="glyphicon glyphicon-ok"></span> Добавить</button>
                        </div>
                    </div>

                </form>
            </div>
        </div>

        <div class="col-sm-6">
            <form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
                {{ csrf_field() }}

                <div class="form-group">
                    <h1>Страница</h1><br>
                </div>

                <div class="form-group">
                    <label for="namePage" class="col-sm-4">Название:</label>

                    <div class="col-sm-6">
                        <input id="namePage" type="text" class="form-control" name="namePage">
                    </div>
                </div>


                    <div class="form-group">
                        <label for="formatPage" class="col-sm-4">Формат страницы:</label>

                        <div class="col-sm-6">
                            <select id="formatPage" class="form-control" name="formatPage">
                                <option>1</option>
                                <option>2</option>
                                <option>3</option>
                            </select>
                        </div>
                    </div>

                <div class="bannerSize">
                    <div class="form-group">
                        <label for="bannerSize" class="col-sm-4">Размер принимаемых баннеров:</label>

                        <div class="col-sm-6">
                            <select id="bannerSize" class="form-control" name="bannerSize">
                                <option>1</option>
                                <option>2</option>
                                <option>3</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group bannerSizeContainer">
                        <label for="addbannerSize" class="col-sm-4 ">Выбранные размеры:</label>
                        <!-- Реализовать на ReactJS добавление в виде кнопок -->
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        <button type="button" class="btn btn-sm btn-success" id="addPage">
                            <span class="glyphicon glyphicon-ok"></span> Добавить к сайту</button>
                    </div>
                </div>

            </form>
        </div>
    </div>
</div>