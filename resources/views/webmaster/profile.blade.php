@extends('modals.infoModal')
@extends('modals.yesNoModal')
@extends('modals.yesModal')


<div class="container webmaster-profile-container">
    <div class="container-fluid profile-head-container">
    <div class="row">
    <div class="col-md-6">
        <h3>КОНТАКТНЫЕ ДАННЫЕ </h3>
        <dl class="dl-horizontal" >
            <dt>Фамилия:</dt>
            <dd>{{ $surname}} </dd>
            <dt>Имя:</dt>
            <dd>{{ $real_name}} </dd>
            <dt>Отчество:</dt>
            <dd>{{ $patronymic}} </dd>
            <dt>Email:</dt>
            <dd>{{ $email}} </dd>
            <dt>QIWI:</dt>
            <dd>
            @if($user_qiwi == "")
            &mdash;
            @else {{ $user_qiwi }}
            @endif
            </dd>
        </dl>
             <button type="button" class="btn btn-default btn-lg btn-msg">
            <span class="glyphicon glyphicon-envelope"></span> Личные сообщения</button>
    </div>

    <div class="col-md-3 buttons-right">
        <button type="button" class="btn btn-default btn-block">
            <span class="glyphicon glyphicon-pencil"></span> Редактировать</button>
        <button type="button" class="btn btn-default btn-block">
            <span class="glyphicon glyphicon-lock"></span> Изменить пароль</button>

    </div>
        <div class="col-md-3"></div>
    </div>
    </div>


    <div class="balance-info">
        <h3>БАЛАНС</h3>
         {{ $balance }}
        <button type="button" class="btn btn-default btn-balance"> <span class="glyphicon glyphicon-credit-card"></span> Пополнить</button>
    </div>
    <div class="banner-applications">
        <h3>ЗАЯВКИ С БАННЕРАМИ</h3>
        <div class="banner-applications-content" data-url="{{url('/webmaster_bannerTable')}} ">
        </div>



    </div>
     <script type="text/javascript" src={{asset('js/banner-application-table.js')}}></script>
</div>
