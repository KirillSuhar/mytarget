<div class="banner-applications-content" data-url="{{url('/webmaster_bannerTable')}} ">
@if(count($response) == 0)
    <span>Заявок нет</span>
@else

<table class="table banner-applications-table  table-bordered">

    @for($i=0; $i < count($response); $i++ )
        <tr>
            <td> Рекламодатель </td>
            <td>  <button type="button" class="btn btn-default btn-lg banner" content="{{ $response[$i] }}" >Баннер</button> </td>
            <td class="table-button"> <button type="button" data-target=".bs-example-modal-sm" class="btn btn-sm btn-success btn-accept" content="{{ $response[$i] }}">
                    <span class="glyphicon glyphicon-ok"></span> Принять</button> </td>
            <td class="table-button"> <button type="button" data-target=".bs-example-modal-sm" class="btn btn-sm btn-danger btn-reject" content="{{ $response[$i] }}">
                    <span class="glyphicon glyphicon-remove"></span> Отклонить</button> </td>
        </tr>
    @endfor

</table>

<div id="pagination">

         @include('pagination.default', ['paginator' => $response])


</div>


{{-- $banner_requests->render() --}}
    <div id="scripts">
        <script type="text/javascript" src={{asset('js/profile-modal-windows.js')}}></script>
        <script type="text/javascript" src={{asset('js/banner-table-pagination.js')}}></script>
    </div>

@endif

</div>
