<div class="container webmaster-profile-container">
    <a>Назад</a>
    <h3>ПОПОЛНЕНИЕ БАЛАНСА</h3>
    <form role="form" data-toggle="validator">
        <div class="col-md-4">
            <div class="form-group">
                <label for="inputSum">Сумма:</label>
                <input type="text" pattern="[0-9]{10}" class="form-control" id="inputSum" placeholder="Введите сумму" required>
            </div>
        </div>
        <button type="submit" class="btn btn-default">ОК</button>
        <button type="reset" class="btn btn-default">ОТМЕНА</button>
    </form>
</div>