<div class="container webmaster-profile-container">
    <a>Назад</a>
    <h3>РЕДАКТИРОВАНИЕ</h3>
    <form role="form" data-toggle="validator" class="col-md-4">
            <div class="form-group">
                <label for="inputName" class="control-label">Имя:</label>
                <input type="text" class="form-control" id="inputName"  pattern="[a-zA-Zа-яА-Я]{1,15}" placeholder="Введите имя" value="{{ $data['real_name']}}" required>
                <div class="help-block with-errors"></div>
            </div>
            <div class="form-group">
                <label for="inputSurname">Фамилия:</label>
                <input type="text" class="form-control" pattern="[a-zA-Zа-яА-Я]{1,20}" id="inputSurname" placeholder="Введите фамилию" value="{{ $data['surname']}}" required>
                <div class="help-block with-errors"></div>
            </div>
            <div class="form-group">
                <label for="inputPatronymic">Отчество:</label>
                <input type="text" class="form-control" id="inputPatronymic" pattern="[a-zA-Zа-яА-Я]{1,20}" placeholder="Введите отчество" value="{{ $data['patronymic']}}" required>
                <div class="help-block with-errors"></div>
            </div>
            <div class="form-group">
                <label for="inputLogin">Логин:</label>
                <input type="text" class="form-control" pattern="^[a-zA-Z][a-zA-Z0-9-_\.]{1,20}$"  id="inputLogin" placeholder="Введите логин" required>
                <div class="help-block with-errors"></div>
            </div>
            <div class="form-group">
                <label for="inputEmail">E-mail:</label>
                <input type="text" class="form-control" pattern="^[-\w.]+@([A-z0-9][-A-z0-9]+\.)+[A-z]{2,4}$" id="inputEmail" placeholder="Введите логин" value="{{ $data['email']}}" required>
                <div class="help-block with-errors"></div>
            </div>
            <div class="form-group">
                <label for="inputQIWI">Счёт QIWI</label>s
                <input type="text" class="form-control" pattern="^( +)?((\+?7|8) ?)?((\(\d{3}\))|(\d{3}))?( )?(\d{3}[\- ]?\d{2}[\- ]?\d{2})( +)?$" id="inputQIWI" placeholder="Введите счёт QIWI" value="{{ isset($data['user_qiwi']) ? $data['user_qiwi'] : '-'}}" required>
                <div class="help-block with-errors"></div>
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-default">ОК</button>
                <button type="reset" class="btn btn-default">ОТМЕНА</button>
            </div>
    </form>
</div>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/1000hz-bootstrap-validator/0.11.5/validator.js"></script>
