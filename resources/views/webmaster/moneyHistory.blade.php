@section('css')
    <link rel="stylesheet" href={{asset('css/moneyHistory.css')}}>
@endsection
<div class="container webmaster-profile-container">
    <h3>ИСТОРИЯ ДЕНЕЖНЫХ ОПЕРАЦИЙ</h3>
    <div class="blocks-applications">
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <tr>
                            <th>Дата</th>
                            <th>Тип операций</th>
                            <th>Сумма</th>
                            <th>Статус</th>
                            <th>Кошелек QIWI</th>
                            <th>ID тразакции</th>
                        </tr>

                        @if (isset($data))
                            @if(empty($data[0]))
                    </table>  <div class="money"> Данных нет</div>
                    @else
                        @foreach( $data as $rec )
                            <tr>
                                <td>{{$rec['created_at']}}</td>
                                <td>{{$rec['type']}}</td>
                                <td>{{$rec['money']}}</td>
                                <td>{{$rec['status']}}</td>
                                <td>{{$rec['user_qiwi']}}</td>
                                <td>{{$rec['qiwi_transaction_id']}}</td>
                            </tr>
                         @endforeach </table>
                            @endif
                            @else </table> <div class="money"> Операций не было  </div>

                            @endif
                </div>
            </div>
        </div>
    </div>
</div>