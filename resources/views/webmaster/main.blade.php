@extends('layouts.app')

@section('title', 'Вебмастер - MyTarget')

@section('css')
    <link rel="stylesheet" href={{asset('css/webmaster.css')}}>
    <link rel="stylesheet" href={{asset('css/moneyHistory.css')}}>
    @endsection


@section('menu')

    <div class="container webmaster-main">

    <h2 id="webmaster-h3">ВЕБМАСТЕР</h2>

    <div class="container-fluid">
        <ul class="nav nav-tabs" id="myTabs">
            <li class="active"><a href="#profile" data-url="{{url('/webmaster_profile')}}">Профиль</a></li>
            <li><a href="#add-site" data-url="{{url('/webmaster_addSite')}}">Добавить сайт</a></li>
            <li><a href="#my-sites" data-url="{{url('/webmaster_mysites')}}">Мои сайты</a></li>
            <li><a href="#withdrawal" data-url="{{url('/webmaster_cashouts')}}">Вывод средств</a></li>
            <li><a href="#money-history" data-url="{{url('/webmaster_moneyHistory')}}">История денежных операций</a></li>
        </ul>

        <div class="tab-content">
            <div class="tab-pane active" id="profile"></div>
            <div class="tab-pane" id="add-site"></div>
            <div class="tab-pane" id="my-sites"></div>
            <div class="tab-pane" id="withdrawal"></div>
            <div class="tab-pane" id="money-history"></div>
        </div>
    </div>

        </div>

    <script type="text/javascript" src={{asset('js/navtab-menu.js')}}></script>

@endsection