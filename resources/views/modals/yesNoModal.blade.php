<!-- Modal yes/no -->
<div class="modal fade bs-example-modal-sm" id="yesNoModal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm yes-no-modal">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body text-center" >
                <h4 id="yesNoModal-content"></h4>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-success btn-default pull-left" id="yesNoModal-btn-ok" data-dismiss="modal"><span class="glyphicon glyphicon-ok"></span> Да&nbsp;</button>
                <button type="submit" class="btn btn-danger btn-default pull-right" id="yesNoModal-btn-cancel" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Нет</button>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript" src={{asset('js/modal.js')}}></script>