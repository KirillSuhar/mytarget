<!-- Modal for banner -->
<div class="modal fade" role="dialog" id="infoModal">
    <div class="modal-dialog info-modal">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 id="infoModal-name"></h4>
            </div>
            <div class="modal-body" id="infoModal-content">
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src={{asset('js/modal.js')}}></script>