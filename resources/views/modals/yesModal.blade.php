
<!-- Modal yes/no -->
<div class="modal fade bs-example-modal-sm" id="yesModal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm yes-modal">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body text-center" >
                <h4 id="yesModal-content"></h4>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-success btn-default center-block" id="yesModal-btn-ok" data-dismiss="modal"><span class="glyphicon glyphicon-ok"></span> ОК&nbsp;</button>

            </div>
        </div>
    </div>
</div>


<script type="text/javascript" src={{asset('js/modal.js')}}></script>