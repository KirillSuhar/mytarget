<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/
use App\Models\Banner;
use App\Models\BannerPlace;
use App\Models\SitePage;
use App\Models\BannerShowRequest;
use App\Common\Enums\BannerShowRequestStatus;

$factory->define(App\Models\BannerShowRequest::class, function (Faker\Generator $faker) {
    return [
        'id_banner' => 1,
        'id_place' => 1,
        'clicks_left' => '10',
        'shows_left' => '25',
        'deadline' => date('Y-m-d H:i:s', strtotime('23-8-2016 23:15:23')),
        'status' => BannerShowRequestStatus::ACCEPTED, // принят
        'created_at' => date('Y-m-d H:i:s'),
        'updated_at' => date('Y-m-d H:i:s')
    ];
});