<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBannersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('banners', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_project')->unsigned();
            $table->integer('id_subject')->unsigned();
            $table->string('name',100)->index();
            $table->string('domain');
            $table->string('size',20);
            $table->integer('click_price')->unsigned();
            $table->integer('show_price')->unsigned();
            $table->integer('max_clicks')->unsigned();
            $table->integer('max_shows')->unsigned();
            $table->tinyInteger('status');
            $table->string('link')->index();
            $table->timestamps();
        });

        Schema::table('banners', function (Blueprint $table) {
            $table->foreign('id_project')->references('id')->on('projects')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->foreign('id_subject')->references('id')->on('banner_subjects')
                ->onDelete('cascade')
                ->onUpdate('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('banners');
    }
}
