<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSiteCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('site_categories', function (Blueprint $table) {
            $table->integer('id_site')->unsigned();
            $table->integer('id_category')->unsigned();
            $table->primary(array('id_site', 'id_category'));
        });

        Schema::table('site_categories', function (Blueprint $table) {
            $table->foreign('id_site')->references('id')->on('sites')
                   ->onUpdate('cascade');
            $table->foreign('id_category')->references('id')->on('categories')
                  ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('site_categories');
    }
}
