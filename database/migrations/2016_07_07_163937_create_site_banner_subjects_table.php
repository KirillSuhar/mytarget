<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSiteBannerSubjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('site_banner_subjects', function (Blueprint $table) {
            $table->integer('id_subject')->unsigned();
            $table->integer('id_site')->unsigned();
            $table->primary(array('id_subject','id_site'));
        });

        Schema::table('site_banner_subjects', function (Blueprint $table) {
           $table->foreign('id_subject')->references('id')->on('banner_subjects')
               ->onDelete('cascade')
               ->onUpdate('cascade');
            $table->foreign('id_site')->references('id')->on('sites')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('site_banner_subjects');
    }
}
