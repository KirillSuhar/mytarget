<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSitesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sites', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_webmaster')->unsigned();
            $table->string('name',100)->index('i_name');
            $table->string('domain');
            $table->text('description');
            $table->tinyInteger('status');
            $table->timestamps();
        });
        Schema::table('sites', function (Blueprint $table) {
            $table->foreign('id_webmaster')->references('id')->on('users')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sites');
    }
}
