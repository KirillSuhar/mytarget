<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSiteStatisticsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('site_statistics', function (Blueprint $table) {
            $table->integer('id')->unsigned();
            $table->primary('id');
            $table->integer('yandex_pages_count')->unsigned();
            $table->float('tic')->unsigned();
            $table->float('ags')->unsigned();
            $table->float('google_page_rank')->unsigned();
            $table->integer('likes')->unsigned();
            $table->float('dmoz')->unsigned();
            $table->date('domen_age');
            $table->timestamps();
        });
        Schema::table('site_statistics', function (Blueprint $table) {
           $table->foreign('id')->references('id')->on('sites')
               ->onDelete('cascade')
               ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('site_statistics');
    }
}
