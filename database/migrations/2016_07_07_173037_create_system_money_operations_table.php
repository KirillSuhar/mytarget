<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSystemMoneyOperationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('system_money_operations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_request')->unsigned();
            $table->dateTime('operation_date');
            $table->integer('money')->unsigned();
            $table->timestamps();
        });
        Schema::table('system_money_operations', function (Blueprint $table) {
            $table->foreign('id_request')->references('id')->on('banner_show_requests');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('system_money_operations');
    }
}
