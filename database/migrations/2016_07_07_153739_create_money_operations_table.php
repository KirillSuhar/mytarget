<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMoneyOperationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('money_operations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_sender')->unsigned();
            $table->tinyInteger('type');
            $table->tinyInteger('status');
            $table->integer('money');
            $table->string('user_qiwi',70);
            $table->string('qiwi_transaction_id',100);
            $table->timestamps();
        });
        Schema::table('money_operations',function(Blueprint $table){
           $table->foreign('id_sender')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('money_operations');
    }
}
