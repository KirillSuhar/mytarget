<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSitePagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('site_pages', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_site')->unsigned();
            $table->string('name',100);
            $table->string('domain');
            $table->timestamps();
        });
        
        Schema::table('site_pages', function (Blueprint $table) {
            $table->foreign('id_site')->references('id')->on('sites')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('site_pages');
    }
}
