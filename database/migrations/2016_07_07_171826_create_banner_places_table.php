<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBannerPlacesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('banner_places', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_page')->unsigned();
            $table->string('size',20);
            $table->tinyInteger('status');
            $table->timestamps();
        });
        
        Schema::table('banner_places', function (Blueprint $table) {
            $table->foreign('id_page')->references('id')->on('site_pages')
                ->onDelete('cascade')
                ->onUpdate('cascade');

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('banner_places');
    }
}
