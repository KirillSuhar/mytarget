<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBannerShowRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('banner_show_requests', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_banner')->unsigned();
            $table->integer('id_place')->unsigned();
            $table->integer('clicks_left');
            $table->integer('shows_left');
            $table->dateTime('deadline');
            $table->tinyInteger('status');
            $table->timestamps();
        });

        Schema::table('banner_show_requests', function (Blueprint $table) {
           $table->foreign('id_banner')->references('id')->on('banners');
           $table->foreign('id_place')->references('id')->on('banner_places');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('banner_show_requests');
    }
}
