<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_advertiser')->unsigned();
            $table->string('name',100);
            $table->text('description');
            $table->tinyInteger('mode');
            $table->tinyInteger('rotation_type');
            $table->integer('max_budget')->unsigned();
            $table->dateTime('deadline');
            $table->integer('max_clicks')->unsigned();
            $table->integer('max_shows')->unsigned();
            $table->tinyInteger('status');
            $table->timestamps();
        });
        
        Schema::table('projects',function (Blueprint $table){

            $table->foreign('id_advertiser')->references('id')->on('users')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('projects');
    }
}
