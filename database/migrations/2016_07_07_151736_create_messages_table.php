<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('messages', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_sender')->unsigned();
            $table->integer('id_receiver')->unsigned();
            $table->tinyInteger('status');
            $table->text('text');
            $table->integer('id_correspondence')->unique();
            $table->timestamps();
        });

        Schema::table('messages',function (Blueprint $table){
            $table->foreign('id_sender')->references('id')->on('users')
                ->onUpdate('cascade');
            $table->foreign('id_receiver')->references('id')->on('users')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('messages');
    }
}
