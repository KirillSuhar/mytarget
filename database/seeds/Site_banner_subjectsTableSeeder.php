<?php

use Illuminate\Database\Seeder;
use App\Models\SiteBannerSubject;
use App\Models\BannerSubject;
use App\Models\Site;
class Site_banner_subjectsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        SiteBannerSubject::create([
            'id_subject' => BannerSubject::where('name','=','живопись')->first()->id,
            'id_site' => Site::where('name','=','Web1Site1')->first()->id,
        ]);
        SiteBannerSubject::create([
            'id_subject' => BannerSubject::where('name','=','рисунок')->first()->id,
            'id_site' => Site::where('name','=','Web1Site1')->first()->id,
        ]);
        SiteBannerSubject::create([
            'id_subject' => BannerSubject::where('name','=','спортивная обувь')->first()->id,
            'id_site' => Site::where('name','=','Web2Site1')->first()->id,
        ]);
        SiteBannerSubject::create([
            'id_subject' => BannerSubject::where('name','=','футбол')->first()->id,
            'id_site' => Site::where('name','=','Web2Site1')->first()->id,
        ]);
        SiteBannerSubject::create([
            'id_subject' => BannerSubject::where('name','=','теннис')->first()->id,
            'id_site' => Site::where('name','=','Web2Site1')->first()->id,
        ]);
        SiteBannerSubject::create([
            'id_subject' => BannerSubject::where('name','=','волейбол')->first()->id,
            'id_site' => Site::where('name','=','Web2Site1')->first()->id,
        ]);
        SiteBannerSubject::create([
            'id_subject' => BannerSubject::where('name','=','мультфильм')->first()->id,
            'id_site' => Site::where('name','=','Web3Site1')->first()->id,
        ]);
        SiteBannerSubject::create([
            'id_subject' => BannerSubject::where('name','=','волейбол')->first()->id,
            'id_site' => Site::where('name','=','sitekarina123')->first()->id,
        ]);
        SiteBannerSubject::create([
            'id_subject' => BannerSubject::where('name','=','мультфильм')->first()->id,
            'id_site' => Site::where('name','=','sitekarina123')->first()->id,
        ]);
    }
}
