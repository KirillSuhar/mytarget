<?php

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Common\Enums\UserRole;
class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Записи рекламодателей
        User::create([
            'name' => 'karina123',
            'email' => 'karina123@mail.ru',
            'password' => 'karina123',
            'remember_token' =>NULL,
            'frozen_balance' => '0',
            'role' =>UserRole::ADVERTISER,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
        User::create([
            'name' => 'Kirill',
            'email' => 'suhorukih@mail.ru',
            'full_name' => 'Сухоруких Кирилл Всеволодович',
            'password' => '$2y$10$GuZjoPAja9Tr/1tATwRVv.kEfXdvABu9Fe3rNKrMqmFf4PJau27Y2',
            'remember_token' =>'o9KgS9g1SFdgZ0CDJTGgfzt9sgkagCP78QlBWNDk8pyfAxIxucb9Vcr5iDWN',
            'frozen_balance' => '0',
            'role' =>UserRole::ADVERTISER,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
        User::create([
            'name' => 'Kostya',
            'email' => 'someaddr@gmail.com',
            'password' => '$2y$10$DyKoWHSt57U.cmuj5tPrWe.b.ROH.HMz1tAAt31GHxEneoS4ykEK6',
            'remember_token' =>'FOkpY6MNo3PyJxuXEDXlUoh1Hp5bMwmVbaPN1ZcbXgVmcEGQJSYzrdk6yWtm',
            'frozen_balance' => '0',
            'role' =>UserRole::ADVERTISER,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
        User::create([
            'name' => 'OneMoreAcc',
            'email' => 'some@gmail.com',
            'password' => '$2y$10$uFwYaVdaAD3uI.8aUtaMU.OryXY9RPPK9hps83SeTDK.xGCsbfcPa',
            'remember_token' =>'oEYdPIDJhFBBRqdkvN79Aik6QKUZz9zGvKVG04Iui3gfDxMrqdUEEo61Zv6x',
            'frozen_balance' => '0',
            'role' =>UserRole::ADVERTISER,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
        //Записи веб-мастеров
        User::create([
            'name' => 'Web1',
            'email' => 'picemki@mail.ru',
            'password' => '12345678',
            'remember_token' =>NULL,
            'frozen_balance' => '0',
            'role' =>UserRole::WEBMASTER,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
        User::create([
            'name' => 'Web2',
            'email' => 'picemki2@mail.ru',
            'password' => '12345678',
            'remember_token' =>NULL,
            'frozen_balance' => '0',
            'role' =>UserRole::WEBMASTER,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
        User::create([
            'name' => 'Web3',
            'email' => 'picemki3@mail.ru',
            'password' => '12345678',
            'remember_token' =>NULL,
            'frozen_balance' => '0',
            'role' =>UserRole::WEBMASTER,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
        //Записи админов
        User::create([
            'name' => 'admin',
            'email' => 'admin@mail.ru',
            'password' => 'admin1234',
            'remember_token' =>NULL,
            'frozen_balance' => '0',
            'role' =>UserRole::ADMIN,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
        // Записи модераторов
        User::create([
            'name' => 'moder',
            'email' => 'moder@mail.ru',
            'password' => 'moder1234',
            'remember_token' =>NULL,
            'frozen_balance' => '0',
            'role' =>UserRole::MODERATOR,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

    }
}
