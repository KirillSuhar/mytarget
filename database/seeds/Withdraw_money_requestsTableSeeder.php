<?php

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\WithdrawMoneyRequest;
use App\Common\Enums\WithdrawMoneyRequestStatus;
class Withdraw_money_requestsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        WithdrawMoneyRequest::create([
            'user_id' => User::where('name','=','Kirill')->first()->id,
            'output_money' => '100000',
            'status' => WithdrawMoneyRequestStatus::DECLINED,//отклонена
            'user_qiwi' => User::where('name','=','Kirill')->first()->user_qiwi,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
        WithdrawMoneyRequest::create([
            'user_id' => User::where('name','=','Kirill')->first()->id,
            'output_money' => '10',
            'status' => WithdrawMoneyRequestStatus::CONSIDER,//непроверена
            'user_qiwi' => User::where('name','=','Kirill')->first()->user_qiwi,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
    }
}
