<?php

use Illuminate\Database\Seeder;
use App\Models\BannerPlace;
use App\Models\SitePage;
use App\Common\Enums;
class Banner_placesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        BannerPlace::create([
            'id_page' => SitePage::where('name','=','Web1Site1Page1')->first()->id,
            'size' => '336 х 280',
            'status' => '1',//занято
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
        BannerPlace::create([
            'id_page' => SitePage::where('name','=','Web1Site1Page2')->first()->id,
            'size' => '336 х 280',
            'status' => '0',//не занято
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
        BannerPlace::create([
            'id_page' => SitePage::where('name','=','Web1Site1Page3')->first()->id,
            'size' => '336 х 280',
            'status' => '0',//не занято
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
        BannerPlace::create([
            'id_page' => SitePage::where('name','=','Web2Site1Page1')->first()->id,
            'size' => '120 х 240',
            'status' => '1',//занято
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
        BannerPlace::create([
            'id_page' => SitePage::where('name','=','Web3Site1Page1')->first()->id,
            'size' => '336 х 280',
            'status' => '0',//не занято
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
        BannerPlace::create([
            'id_page' => SitePage::where('name','=','sitekarina123Page1')->first()->id,
            'size' => '120 х 240',
            'status' => '0',//не занято
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
        BannerPlace::create([
            'id_page' => SitePage::where('name','=','sitekarina123Page1')->first()->id,
            'size' => '336 х 280',
            'status' => '0',//не занято
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
        BannerPlace::create([
            'id_page' => SitePage::where('name','=','sitekarina123Page2')->first()->id,
            'size' => '120 х 240',
            'status' => '0',//не занято
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
        BannerPlace::create([
            'id_page' => SitePage::where('name','=','sitekarina123Page2')->first()->id,
            'size' => '336 х 280',
            'status' => '0',//не занято
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
        BannerPlace::create([
            'id_page' => SitePage::where('name','=','sitekarina123Page3')->first()->id,
            'size' => '120 х 240',
            'status' => '0',//не занято
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
        BannerPlace::create([
            'id_page' => SitePage::where('name','=','sitekarina123Page3')->first()->id,
            'size' => '336 х 280',
            'status' => '0',//не занято
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
    }
}
