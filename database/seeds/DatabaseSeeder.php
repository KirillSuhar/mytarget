<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        $this->call(UsersTableSeeder::class);
        $this->call(Withdraw_money_requestsTableSeeder::class);
        $this->call(MessagesTableSeeder::class);
        $this->call(Money_operationsTableSeeder::class);
        $this->call(ProjectsTableSeeder::class);
        $this->call(FiltersTableSeeder::class);
        $this->call(CategoriesTableSeeder::class);
        $this->call(SitesTableSeeder::class);
        $this->call(Site_statisticsTableSeeder::class);
        $this->call(Banner_subjectsTableSeeder::class);
        $this->call(Site_banner_subjectsTableSeeder::class);
        $this->call(BannersTableSeeder::class);
        $this->call(Site_pagesTableSeeder::class);
        $this->call(Banner_placesTableSeeder::class);
        $this->call(Banner_show_requestsTableSeeder::class);
        $this->call(System_money_operationsTableSeeder::class);
    }
}
