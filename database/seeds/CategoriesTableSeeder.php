<?php

use Illuminate\Database\Seeder;
use App\Models\Category;
class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        Category::create([
            'parent_id' => NULL,
            'name' => 'культура',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
        Category::create([
            'parent_id' => Category::where('name','=','культура')->first()->id,
            'name' => 'искусство',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
        Category::create([
            'parent_id' => Category::where('name','=','культура')->first()->id,
            'name' => 'архитектура',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
        Category::create([
            'parent_id' => Category::where('name','=','архитектура')->first()->id,
            'name' => 'архитектура барокко',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
        Category::create([
            'parent_id' => Category::where('name','=','архитектура')->first()->id,
            'name' => 'готическая архитектура',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
        Category::create([
            'parent_id' => Category::where('name','=','культура')->first()->id,
            'name' => 'кино',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
        Category::create([
            'parent_id' => NULL,
            'name' => 'спорт',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
        Category::create([
            'parent_id' => Category::where('name','=','спорт')->first()->id,
            'name' => 'спортивные игры',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
        Category::create([
            'parent_id' => NULL,
            'name' => 'другое',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

    }
}
