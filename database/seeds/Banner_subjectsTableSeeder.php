<?php

use Illuminate\Database\Seeder;
use App\Models\BannerSubject;
class Banner_subjectsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        BannerSubject::create([
            'name' => 'живопись',
        ]);
        BannerSubject::create([
            'name' => 'рисунок',
        ]);
        BannerSubject::create([
            'name' => 'мультфильм',
        ]);
        BannerSubject::create([
            'name' => 'футбол',
        ]);
        BannerSubject::create([
            'name' => 'волейбол',
        ]);
        BannerSubject::create([
            'name' => 'спортивная обувь',
        ]);
        BannerSubject::create([
            'name' => 'теннис',
        ]);
        BannerSubject::create([
            'name' => 'недвижимость',
        ]);
        BannerSubject::create([
            'name' => 'программное обеспечение',
        ]);
        BannerSubject::create([
            'name' => 'техническое обеспечение',
        ]);
        BannerSubject::create([
            'name' => 'машины',
        ]);
        BannerSubject::create([
            'name' => 'мотоциклы',
        ]);
    }
}
