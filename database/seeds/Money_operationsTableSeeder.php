<?php

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\MoneyOperation;
use App\Common\Enums\MoneyOperationStatus;
class Money_operationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        MoneyOperation::create([
            'id_sender' => User::where('name','=','Kirill')->first()->id,
            'type' => '0',//пополнение
            'status' => MoneyOperationStatus::WAITING,//непроверена
            'money' => '1000',
            'user_qiwi' => User::where('name','=','Kirill')->first()->user_qiwi,
            'qiwi_transaction_id' => rand(0,99999999),
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
        MoneyOperation::create([
            'id_sender' => User::where('name','=','Kostya')->first()->id,
            'type' => '0',//пополнение
            'status' => MoneyOperationStatus::WAITING,//непроверена
            'money' => '1000',
            'user_qiwi' => User::where('name','=','Kostya')->first()->user_qiwi,
            'qiwi_transaction_id' => rand(0,99999999),
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
    }
}
