<?php

use Illuminate\Database\Seeder;
use App\Models\SitePage;
use App\Models\Site;
class Site_pagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        SitePage::create([
            'id_site' => Site::where('name','=','Web1Site1')->first()->id,
            'name' => 'Web1Site1Page1',
            'domain' => '.com',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
        SitePage::create([
            'id_site' => Site::where('name','=','Web1Site1')->first()->id,
            'name' => 'Web1Site1Page2',
            'domain' => '.com',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
        SitePage::create([
            'id_site' => Site::where('name','=','Web1Site1')->first()->id,
            'name' => 'Web1Site1Page3',
            'domain' => '.com',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
        SitePage::create([
            'id_site' => Site::where('name','=','Web2Site1')->first()->id,
            'name' => 'Web2Site1Page1',
            'domain' => '.ru',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
        SitePage::create([
            'id_site' => Site::where('name','=','Web3Site1')->first()->id,
            'name' => 'Web3Site1Page1',
            'domain' => '.ru',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
        SitePage::create([
            'id_site' => Site::where('name','=','sitekarina123')->first()->id,
            'name' => 'sitekarina123Page1',
            'domain' => '.com',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
        SitePage::create([
            'id_site' => Site::where('name','=','sitekarina123')->first()->id,
            'name' => 'sitekarina123Page2',
            'domain' => '.com',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
        SitePage::create([
            'id_site' => Site::where('name','=','sitekarina123')->first()->id,
            'name' => 'sitekarina123Page3',
            'domain' => '.com',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

    }
}
