<?php

use Illuminate\Database\Seeder;
use App\Models\Message;
use App\Models\User;
use App\Common\Enums\MessageStatus;
class MessagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run()
    {
        $faker = Faker\Factory::create();
        Message::create([
            'id_sender' => User::where('name','=','Kirill')->first()->id,
            'id_receiver' => User::where('name','=','Kostya')->first()->id,
            'status' => MessageStatus::READ,
            'text' => $faker->text(50),
            'id_correspondence' => '0',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Message::create([
            'id_sender' => User::where('name','=','Kirill')->first()->id,
            'id_receiver' => User::where('name','=','Kostya')->first()->id,
            'status' => MessageStatus::UNREAD,
            'text' => $faker->text(50),
            'id_correspondence' => '1',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Message::create([
            'id_sender' => User::where('name','=','Kostya')->first()->id,
            'id_receiver' => User::where('name','=','Kirill')->first()->id,
            'status' => MessageStatus::UNREAD,
            'text' => $faker->text(50),
            'id_correspondence' => '2',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
    }
}
