<?php

use Illuminate\Database\Seeder;
use App\Models\Banner;
use App\Models\Project;
use App\Models\BannerSubject;
use App\Common\Enums\BannerStatus;
class BannersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        Banner::create([
            'id_project' => Project::where('name','=','KirillsProject1')->first()->id,
            'id_subject' => BannerSubject::where('name','=','живопись')->first()->id,
            'name' => 'KirillsPr1Ban1',
            'domain' => '.com',
            'size' => '336 х 280',
            'click_price' => '20',
            'show_price' => '10',
            'max_clicks' => '10',
            'max_shows' => '25',
            'status' => BannerStatus::ACTIVE, // активен
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
        Banner::create([
            'id_project' => Project::where('name','=','KirillsProject1')->first()->id,
            'id_subject' => BannerSubject::where('name','=','рисунок')->first()->id,
            'name' => 'KirillsPr1Ban2',
            'domain' => '.com',
            'size' => '336 х 280',
            'click_price' => '20',
            'show_price' => '10',
            'max_clicks' => '10',
            'max_shows' => '25',
            'status' => BannerStatus::ACTIVE, // активен
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
        Banner::create([
            'id_project' => Project::where('name','=','KirillsProject2')->first()->id,
            'id_subject' => BannerSubject::where('name','=','волейбол')->first()->id,
            'name' => 'KirillsPr2Ban1',
            'domain' => '.ru',
            'size' => '120 х 240',
            'click_price' => '20',
            'show_price' => '10',
            'max_clicks' => '10',
            'max_shows' => '20',
            'status' => BannerStatus::ACTIVE, // активен
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
        Banner::create([
            'id_project' => Project::where('name','=','KirillsProject2')->first()->id,
            'id_subject' => BannerSubject::where('name','=','теннис')->first()->id,
            'name' => 'KirillsPr2Ban2',
            'domain' => '.ru',
            'size' => '120 х 240',
            'click_price' => '20',
            'show_price' => '10',
            'max_clicks' => '10',
            'max_shows' => '10',
            'status' => BannerStatus::ACTIVE, // активен
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
        Banner::create([
            'id_project' => Project::where('name','=','KirillsProject2')->first()->id,
            'id_subject' => BannerSubject::where('name','=','футбол')->first()->id,
            'name' => 'KirillsPr2Ban3',
            'domain' => '.ru',
            'size' => '120 х 240',
            'click_price' => '20',
            'show_price' => '10',
            'max_clicks' => '10',
            'max_shows' => '10',
            'status' => BannerStatus::ACTIVE, // активен
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
        Banner::create([
            'id_project' => Project::where('name','=','KostyasProject1')->first()->id,
            'id_subject' => BannerSubject::where('name','=','мотоциклы')->first()->id,
            'name' => 'KostyasPr1Ban1',
            'domain' => '.ru',
            'size' => '180 х 150',
            'click_price' => '30',
            'show_price' => '25',
            'max_clicks' => '20',
            'max_shows' => '50',
            'status' => BannerStatus::ACTIVE, // активен
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
        Banner::create([
            'id_project' => Project::where('name','=','KostyasProject1')->first()->id,
            'id_subject' => BannerSubject::where('name','=','машины')->first()->id,
            'name' => 'KostyasPr1Ban2',
            'domain' => '.ru',
            'size' => '180 х 150',
            'click_price' => '30',
            'show_price' => '25',
            'max_clicks' => '20',
            'max_shows' => '50',
            'status' => BannerStatus::INACTIVE, // не активен
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
    }
}
