<?php

use Illuminate\Database\Seeder;
use App\Models\Banner;
use App\Models\BannerPlace;
use App\Models\SitePage;
use App\Models\BannerShowRequest;
use App\Common\Enums\BannerShowRequestStatus;



class Banner_show_requestsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run()
    {
        //$faker = Faker\Factory::create();

        //factory(BannerShowRequest::class,10000)->create();

        BannerShowRequest::create([
            'id_banner' => Banner::where('name', '=', 'KirillsPr1Ban1')->first()->id,
            'id_place' => BannerPlace::where('id', '=', SitePage::where('name', '=', 'Web1Site1Page1')->first()->id)->first()->id,
            'clicks_left' => '10',
            'shows_left' => '25',
            'deadline' => date('Y-m-d H:i:s', strtotime('23-8-2016 23:15:23')),
            'status' => BannerShowRequestStatus::ACCEPTED, // принят
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);


        BannerShowRequest::create([
            'id_banner' => Banner::where('name','=','KirillsPr1Ban2')->first()->id,
            'id_place' => BannerPlace::where('id','=',SitePage::where('name','=','Web1Site1Page1')->first()->id)->first()->id,
            'clicks_left' => '10',
            'shows_left' => '25',
            'deadline' => date('Y-m-d H:i:s', strtotime('23-8-2016 23:15:23')),
            'status' => BannerShowRequestStatus::ACCEPTED, // принят
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
        BannerShowRequest::create([
            'id_banner' => Banner::where('name','=','KirillsPr1Ban1')->first()->id,
            'id_place' => BannerPlace::where('id','=',SitePage::where('name','=','Web1Site1Page2')->first()->id)->first()->id,
            'clicks_left' => '10',
            'shows_left' => '25',
            'deadline' => date('Y-m-d H:i:s', strtotime('23-8-2016 23:15:23')),
            'status' => BannerShowRequestStatus::DECLINED, // отклоненн
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
        BannerShowRequest::create([
            'id_banner' => Banner::where('name','=','KirillsPr1Ban1')->first()->id,
            'id_place' => BannerPlace::where('id','=',SitePage::where('name','=','Web1Site1Page3')->first()->id)->first()->id,
            'clicks_left' => '10',
            'shows_left' => '25',
            'deadline' => date('Y-m-d H:i:s', strtotime('23-8-2016 23:15:23')),
            'status' => BannerShowRequestStatus::UNTREATED, // не рассмотрен
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
        BannerShowRequest::create([
            'id_banner' => Banner::where('name','=','KirillsPr2Ban1')->first()->id,
            'id_place' => BannerPlace::where('id','=',SitePage::where('name','=','Web2Site1Page1')->first()->id)->first()->id,
            'clicks_left' => '10',
            'shows_left' => '15',
            'deadline' => date('Y-m-d H:i:s', strtotime('23-8-2016 23:15:23')),
            'status' => BannerShowRequestStatus::UNTREATED, // принят
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
        BannerShowRequest::create([
            'id_banner' => Banner::where('name','=','KirillsPr2Ban1')->first()->id,
            'id_place' => BannerPlace::where('id','=',SitePage::where('name','=','sitekarina123page1')->first()->id)->first()->id,
            'clicks_left' => '10',
            'shows_left' => '15',
            'deadline' => date('Y-m-d H:i:s', strtotime('23-8-2016 23:15:23')),
            'status' => BannerShowRequestStatus::UNTREATED, // не рассмотрен
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
        BannerShowRequest::create([
            'id_banner' => Banner::where('name','=','KirillsPr2Ban1')->first()->id,
            'id_place' => BannerPlace::where('id','=',SitePage::where('name','=','sitekarina123page2')->first()->id)->first()->id,
            'clicks_left' => '20',
            'shows_left' => '10',
            'deadline' => date('Y-m-d H:i:s', strtotime('23-8-2016 23:15:23')),
            'status' => BannerShowRequestStatus::UNTREATED, // не рассмотрен
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
        BannerShowRequest::create([
            'id_banner' => Banner::where('name','=','KirillsPr1Ban1')->first()->id,
            'id_place' => BannerPlace::where('id','=',SitePage::where('name','=','sitekarina123page1')->first()->id)->first()->id,
            'clicks_left' => '10',
            'shows_left' => '15',
            'deadline' => date('Y-m-d H:i:s', strtotime('23-8-2016 23:15:23')),
            'status' => '0', // не рассмотрен
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
        BannerShowRequest::create([
            'id_banner' => Banner::where('name','=','KirillsPr1Ban1')->first()->id,
            'id_place' => BannerPlace::where('id','=',SitePage::where('name','=','sitekarina123page2')->first()->id)->first()->id,
            'clicks_left' => '20',
            'shows_left' => '10',
            'deadline' => date('Y-m-d H:i:s', strtotime('23-8-2016 23:15:23')),
            'status' => BannerShowRequestStatus::UNTREATED, // не рассмотрен
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
        BannerShowRequest::create([
            'id_banner' => Banner::where('name','=','KirillsPr2Ban1')->first()->id,
            'id_place' => BannerPlace::where('id','=',SitePage::where('name','=','sitekarina123page3')->first()->id)->first()->id,
            'clicks_left' => '10',
            'shows_left' => '15',
            'deadline' => date('Y-m-d H:i:s', strtotime('23-8-2016 23:15:23')),
            'status' => BannerShowRequestStatus::UNTREATED, // не рассмотрен
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
        BannerShowRequest::create([
            'id_banner' => Banner::where('name','=','KirillsPr1Ban1')->first()->id,
            'id_place' => BannerPlace::where('id','=',SitePage::where('name','=','sitekarina123page3')->first()->id)->first()->id,
            'clicks_left' => '20',
            'shows_left' => '10',
            'deadline' => date('Y-m-d H:i:s', strtotime('23-8-2016 23:15:23')),
            'status' => BannerShowRequestStatus::UNTREATED, // не рассмотрен
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);


    }
}
