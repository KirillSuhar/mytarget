<?php

use Illuminate\Database\Seeder;
use App\Models\Site;
use App\Models\User;
use App\Models\Category;
use App\Common\Enums\SiteStatus;
class SitesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        Site::create([
            'id_webmaster' => User::where('name','=','Web1')->first()->id,
            'name' => 'Web1Site1',
            'domain' => '.com',
            'description' => $faker->text(40),
            'status' => SiteStatus::ACTIVE, // прошел проверку
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
        Site::create([
            'id_webmaster' => User::where('name','=','Web1')->first()->id,
            'name' => 'Web1Site2',
            'domain' => '.com',
            'description' => $faker->text(40),
            'status' => SiteStatus::ACTIVE, // прошел проверку
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
        Site::create([
            'id_webmaster' => User::where('name','=','Web1')->first()->id,
            'name' => 'Web1Site3',
            'domain' => '.com',
            'description' => $faker->text(40),
            'status' => SiteStatus::ACTIVE, // прошел проверку
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
        Site::create([
            'id_webmaster' => User::where('name','=','Web1')->first()->id,
            'name' => 'Web1Site4',
            'domain' => '.com',
            'description' => $faker->text(40),
            'status' => SiteStatus::ONMODERATION,//непроверен
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
        Site::create([
        'id_webmaster' => User::where('name','=','Web1')->first()->id,
        'name' => 'Web1Site5',
        'domain' => '.ededededed',
        'description' => $faker->text(40),
        'status' => SiteStatus::ONMODERATION,
        'created_at' => date('Y-m-d H:i:s'),
        'updated_at' => date('Y-m-d H:i:s')
    ]);
        Site::create([
            'id_webmaster' => User::where('name','=','Web1')->first()->id,
            'name' => 'Web1Site6',
            'domain' => '.выаываываыва',
            'description' => $faker->text(40),
            'status' => SiteStatus::ONMODERATION,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        Site::create([
            'id_webmaster' =>User::where('name','=','Web2')->first()->id,
            'name' => 'Web2Site1',
            'domain' => '.ru',
            'description' => $faker->text(40),
            'status' => SiteStatus::ACTIVE, // прошел проверку
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
        Site::create([
            'id_webmaster' => User::where('name','=','Web3')->first()->id,
            'name' => 'Web3Site1',
            'domain' => '.ru',
            'description' => $faker->text(40),
            'status' => SiteStatus::ACTIVE, // прошел проверку
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
        Site::create([
            'id_webmaster' => User::where('name','=','karina123')->first()->id,
            'name' => 'sitekarina123',
            'domain' => '.com',
            'description' => $faker->text(40),
            'status' => '1', // прошел проверку
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
    }
}
