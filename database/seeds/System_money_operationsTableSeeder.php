<?php

use Illuminate\Database\Seeder;
use App\Models\BannerShowRequest;
use App\Models\Banner;
use App\Models\SystemMoneyOperation;
class System_money_operationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        SystemMoneyOperation::create([
            'id_request' => BannerShowRequest::where('id','=',Banner::where('name','=','KirillsPr2Ban1')->first()->id)->first()->id,
            'operation_date' => date('Y-m-d H:i:s'),
            'money' => '10',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
    }   
}
