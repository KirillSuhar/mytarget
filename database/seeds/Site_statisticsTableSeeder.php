<?php

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Site;
use App\Models\SiteStatistics;
class Site_statisticsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        SiteStatistics::create([
            'id' => Site::where('name','=','Web1Site1')->first()->id,
            'yandex_pages_count' => rand(0,9999999),
            'tic' => $faker->randomFloat(2,0,null),
            'ags' => $faker->randomFloat(2,0,null),
            'google_page_rank' => $faker->randomFloat(2,0,null),
            'likes' => rand(0,9999999),
            'dmoz' => $faker->randomFloat(2,0,null),
            'domen_age' => $faker->date('Y-m-d','10-0-0'),
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
        SiteStatistics::create([
            'id' => Site::where('name','=','Web2Site1')->first()->id,
            'yandex_pages_count' => rand(0,9999999),
            'tic' => $faker->randomFloat(2,0,null),
            'ags' => $faker->randomFloat(2,0,null),
            'google_page_rank' => $faker->randomFloat(2,0,null),
            'likes' => rand(0,9999999),
            'dmoz' => $faker->randomFloat(2,0,null),
            'domen_age' => $faker->date('Y-m-d','10-0-0'),
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
        SiteStatistics::create([
            'id' => Site::where('name','=','Web3Site1')->first()->id,
            'yandex_pages_count' => rand(0,9999999),
            'tic' => $faker->randomFloat(2,0,null),
            'ags' => $faker->randomFloat(2,0,null),
            'google_page_rank' => $faker->randomFloat(2,0,null),
            'likes' => rand(0,9999999),
            'dmoz' => $faker->randomFloat(2,0,null),
            'domen_age' => $faker->date('Y-m-d','10-0-0'),
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
    }
}
