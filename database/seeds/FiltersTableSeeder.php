<?php

use Illuminate\Database\Seeder;
use App\Models\Project;
use App\Models\Filter;
class FiltersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        Filter::create([
            'filters_list' => $faker->text(20),
            'id_project' => Project::where('name','=','KirillsProject1')->first()->id,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
        Filter::create([
            'filters_list' => $faker->text(20),
            'id_project' => Project::where('name','=','KirillsProject2')->first()->id,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
        Filter::create([
            'filters_list' => $faker->text(20),
            'id_project' => Project::where('name','=','KirillsProject3')->first()->id,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
        Filter::create([
            'filters_list' => $faker->text(20),
            'id_project' => Project::where('name','=','KostyasProject1')->first()->id,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
    }
}
