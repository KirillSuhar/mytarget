<?php

use Illuminate\Database\Seeder;
use App\Models\Project;
use App\Models\User;
use App\Common\Enums\ProjectStatus;
class ProjectsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        Project::create([
            'id_advertiser' => User::where('name','=','Kirill')->first()->id,
            'name' => 'KirillsProject1',
            'description' => $faker->text(50),
            'mode' => '0', // ручной
            'rotation_type' => '0', //за клик
            'max_budget' => '5000',
            'deadline' => date('Y-m-d H:i:s', strtotime('23-8-2016 23:15:23')),
            'max_clicks' => '20',
            'max_shows' => '50',
            'status' => ProjectStatus::ACTIVE, // активен
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
        Project::create([
            'id_advertiser' => User::where('name','=','Kirill')->first()->id,
            'name' => 'KirillsProject2',
            'description' => $faker->text(50),
            'mode' => '1', // полуавтомат
            'rotation_type' => '1', //за показ
            'max_budget' => '4300',
            'deadline' => date('Y-m-d H:i:s', strtotime('13-9-2016 00:00:00')),
            'max_clicks' => '30',
            'max_shows' => '40',
            'status' => ProjectStatus::ACTIVE, // активен
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
        Project::create([
            'id_advertiser' => User::where('name','=','Kirill')->first()->id,
            'name' => 'KirillsProject3',
            'description' => $faker->text(50),
            'mode' => '2', //автомат
            'rotation_type' => '0', //за клик
            'max_budget' => '4300',
            'deadline' => date('Y-m-d H:i:s', strtotime('13-9-2016 00:00:00')),
            'max_clicks' => '20',
            'max_shows' => '50',
            'status' => ProjectStatus::INACTIVE, // не активен
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
        Project::create([
            'id_advertiser' => User::where('name','=','Kostya')->first()->id,
            'name' => 'KostyasProject1',
            'description' => $faker->text(50),
            'mode' => '0', // ручной
            'rotation_type' => '1', //за показ
            'max_budget' => '5000',
            'deadline' => date('Y-m-d H:i:s', strtotime('23-8-2016 23:15:23')),
            'max_clicks' => '20',
            'max_shows' => '50',
            'status' => ProjectStatus::ACTIVE, // активен
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]); 
    }
}
